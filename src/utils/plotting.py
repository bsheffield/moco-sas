import PIL
import pandas as pd
import torch
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from PIL import Image
import umap
from scipy.signal import welch
from scipy.stats import skew, kurtosis
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import io
import numpy as np
import matplotlib.pyplot as plt
from io import BytesIO
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import roc_curve, auc
import wandb
import plotly.io as pio
import os
from src.datasets.SASAugmentations import SpeckleNoise
from src.datasets.SASDataset import SASDataset
from PIL import Image
from kornia import augmentation as KA
from src.datasets.SASAugmentations import SASDataAugmentation

def normalize_gradients(gradients):

    gradients = gradients - gradients.min()  # Shift the gradient values so they are all positive
    gradients = gradients / gradients.max()  # Scale the gradient values to be between 0 and 1
    return gradients


def fig_to_pil(fig):

    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    buf.seek(0)
    img = PIL.Image.open(buf)
    return [img]


def plot_umap(embeddings, labels, epoch):

    reducer = umap.UMAP()
    embeddings_2d = reducer.fit_transform(embeddings)

    fig, ax = plt.subplots()
    scatter = ax.scatter(embeddings_2d[:, 0], embeddings_2d[:, 1], c=labels, cmap='viridis', s=10)
    ax.set_aspect('equal', 'datalim')
    cbar = plt.colorbar(scatter, boundaries=np.arange(len(np.unique(labels)) + 1) - 0.5, ax=ax)
    cbar.set_ticks(np.unique(labels))
    ax.set_title(f'UMAP projection of the embeddings at epoch {epoch}', fontsize=16)

    return fig


def plot_ig(sas_img, y_probs, integrated_gradients):

    prediction_score, pred_label_idx = torch.topk(y_probs, 1)
    pred_label_idx.squeeze_()

    attributions_ig = integrated_gradients.attribute(sas_img, target=pred_label_idx, n_steps=50)

    # Now visualize the attributions
    attributions_ig = attributions_ig.squeeze().detach().cpu().numpy()
    attributions_ig = normalize_gradients(attributions_ig)

    # Visualize the original sas_imagery
    sas_img = sas_img.squeeze().detach().cpu().numpy()

    # Create a subplot with 2 rows and 2 columns
    fig = make_subplots(rows=2, cols=2,
                        subplot_titles=("LF Original", "HF Original", "LF Attributions", "HF Attributions"))

    # Top row for original images
    fig.add_trace(go.Heatmap(z=sas_img[0], colorscale='Viridis', showscale=True), row=1, col=1)
    fig.add_trace(go.Heatmap(z=sas_img[1], colorscale='Viridis', showscale=True), row=1, col=2)

    # Bottom row for attributions
    fig.add_trace(go.Heatmap(z=attributions_ig[0], colorscale='Viridis', zmin=0, zmax=1, showscale=True), row=2,
                  col=1)
    fig.add_trace(go.Heatmap(z=attributions_ig[1], colorscale='Viridis', zmin=0, zmax=1, showscale=True), row=2,
                  col=2)

    fig.update_layout(width=600, height=600, title_text='LF and HF Originals (top) and Attributions (bottom)')
    fig.show()


def plot_curves(y_true, y_probs):

    precision, recall, _ = precision_recall_curve(y_true, y_probs)
    np.savez('pr_data.npz', precision=precision, recall=recall)
    wandb.save('pr_data.npz')

    fpr, tpr, _ = roc_curve(y_true, y_probs)
    np.savez('roc_data.npz', fpr=fpr, tpr=tpr)
    wandb.save('roc_data.npz')
    roc_auc = auc(fpr, tpr)

    # Plot the ROC curve
    fig1 = go.Figure(data=[
        go.Scatter(x=fpr, y=tpr, mode='lines', name='ROC curve (area = %0.2f)' % roc_auc),
        go.Scatter(x=[0, 1], y=[0, 1], mode='lines', name='Random', line=dict(dash='dash')),
    ])
    fig1.update_layout(
        xaxis_title='False Positive Rate',
        yaxis_title='True Positive Rate',
        # title='Receiver Operating Characteristic',
    )

    fig2 = go.Figure(data=[
        go.Scatter(x=recall, y=precision, mode='lines'),
    ])
    fig2.update_layout(
        xaxis_title='Recall',
        yaxis_title='Precision',
        # title='Precision-Recall Curve',
    )

    img_bytes1 = pio.to_image(fig1, format='png')
    img_bytes2 = pio.to_image(fig2, format='png')

    pil_image1 = Image.open(BytesIO(img_bytes1))
    pil_image2 = Image.open(BytesIO(img_bytes2))

    wandb.log({"ROC Curve": wandb.Image(pil_image1)})
    wandb.log({"PR Curve": wandb.Image(pil_image2)})


def plot_conf_matrix(confmat):
    fig = go.Figure(data=go.Heatmap(
        z=confmat,
        x=['Predicted Negative', 'Predicted Positive'],
        y=['Actual Negative', 'Actual Positive'],
        colorscale='Viridis'))

    fig.update_layout(
        # title='Confusion Matrix',
        xaxis_nticks=2,
        yaxis_nticks=2)

    img_bytes = pio.to_image(fig, format='png')
    pil_image = Image.open(BytesIO(img_bytes))
    wandb.log({"Confusion Matrix": wandb.Image(pil_image)})


def plot_features(features, y_true, logger, title=""):
    # Use PCA to reduce the dimensionality of the embeddings
    pca = PCA(n_components=2)
    pca_features = pca.fit_transform(features)

    # Log PCA plot
    fig, ax = plt.subplots()
    scatter = ax.scatter(pca_features[:, 0], pca_features[:, 1], c=y_true, cmap='tab10')
    # legend1 = ax.legend(*scatter.legend_elements(), title="Classes")
    # ax.add_artist(legend1)
    ax.axis('off')
    logger.log_image(title + " " + "PCA", fig_to_pil(fig))

    # Apply t-SNE on the features
    tsne = TSNE(n_components=2, random_state=42)
    train_features_2d = tsne.fit_transform(features)

    # Plot the 2D features with t-SNE
    fig, ax = plt.subplots()
    ax.scatter(train_features_2d[:, 0], train_features_2d[:, 1], c=y_true, cmap='viridis', alpha=0.5)
    # legend1 = ax.legend(*scatter.legend_elements(), title="Classes")
    # ax.add_artist(legend1)
    ax.set_xlabel('t-SNE 1')
    ax.set_ylabel('t-SNE 2')
    ax.axis('off')
    logger.log_image(title + " " + "t-SNE Visualization of Features", fig_to_pil(fig))

    # Apply UMAP on the features
    reducer = umap.UMAP(n_components=2, random_state=42)
    train_features_2d = reducer.fit_transform(features)

    # Plot the 2D features with UMAP
    fig, ax = plt.subplots()
    ax.scatter(train_features_2d[:, 0], train_features_2d[:, 1], c=y_true, cmap='viridis', alpha=0.5)
    # legend1 = ax.legend(*scatter.legend_elements(), title="Classes")
    # ax.add_artist(legend1)
    ax.set_xlabel('UMAP 1')
    ax.set_ylabel('UMAP 2')
    ax.axis('off')
    logger.log_image(title + " " + "UMAP Visualization of Features", fig_to_pil(fig))

    # Apply t-SNE on the features with n_components=3
    tsne = TSNE(n_components=3, random_state=42)
    train_features_3d = tsne.fit_transform(features)

    # Plot the 3D features with t-SNE
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    scatter = ax.scatter(train_features_3d[:, 0], train_features_3d[:, 1], train_features_3d[:, 2],
                         c=y_true, cmap='viridis', alpha=0.5)
    # legend1 = ax.legend(*scatter.legend_elements(), title="Classes")
    # ax.add_artist(legend1)
    ax.set_xlabel('t-SNE 1')
    ax.set_ylabel('t-SNE 2')
    ax.set_zlabel('t-SNE 3')
    ax.set_axis_off()
    logger.log_image(title + " " + "3D t-SNE Visualization of Features", fig_to_pil(fig))

    # Apply UMAP on the features with n_components=3
    reducer = umap.UMAP(n_components=3, random_state=42)
    train_features_3d = reducer.fit_transform(features)

    # Plot the 3D features with UMAP
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    scatter = ax.scatter(train_features_3d[:, 0], train_features_3d[:, 1], train_features_3d[:, 2],
                         c=y_true, cmap='viridis', alpha=0.5)
    # legend1 = ax.legend(*scatter.legend_elements(), title="Classes")
    # ax.add_artist(legend1)
    ax.set_axis_off()  # Hide axes
    logger.log_image(title + " " + "3D UMAP Visualization of Features", fig_to_pil(fig))

def plot_batch_images(x, y, y_pred, logger, num_images=4):
    x = x.cpu().numpy()
    y = y.cpu().numpy()
    y_pred = y_pred.cpu().numpy()

    fig, axes = plt.subplots(num_images, 2, figsize=(8, num_images * 4))

    for i in range(num_images):
        sample_img = (x[i] - x[i].min()) / (x[i].max() - x[i].min())  # Rescale image values to [0, 1]

        axes[i, 0].imshow(sample_img[0], cmap='viridis')
        axes[i, 0].set_title(f"Channel 1, GT: {y[i]}, Pred: {np.argmax(y_pred[i])}")
        axes[i, 0].axis("off")

        if sample_img.shape[0] > 1:  # Check for a second channel
            axes[i, 1].imshow(sample_img[1], cmap='seismic')
            axes[i, 1].set_title(f"Channel 2, GT: {y[i]}, Pred: {np.argmax(y_pred[i])}")
            axes[i, 1].axis("off")
        else:
            axes[i, 1].remove()  # Remove the unused subplot

    fig.tight_layout()

    img_buf = io.BytesIO()
    plt.savefig(img_buf, format='png')
    plt.close(fig)

    im = Image.open(img_buf)
    logger.log_image("Test Predictions", [im])
    img_buf.close()
    im.close()

def save_sas_imagery_plots(dataset, output_dir="sas_plots", cmap_color='inferno'):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for i in range(len(dataset)):
        sas_img, label = dataset[i]
        sas_img_np = sas_img.numpy()
        label_str = str(label)

        num_channels = sas_img_np.shape[0]

        if num_channels == 1:
            fig, axs = plt.subplots(1, 1, figsize=(5, 5))
            axs.imshow(sas_img_np[0], cmap=cmap_color)
            axs.set_title('HF Channel - Ground Truth: ' + label_str)
            axs.axis('off') # hide the x and y axis
        else:
            fig, axs = plt.subplots(1, 2, figsize=(10, 5))
            axs[0].imshow(sas_img_np[0], cmap=cmap_color)
            axs[0].set_title('LF Channel - Ground Truth: ' + label_str)
            axs[0].axis('off') # hide the x and y axis
            axs[1].imshow(sas_img_np[1], cmap=cmap_color)
            axs[1].set_title('HF Channel - Ground Truth: ' + label_str)
            axs[1].axis('off') # hide the x and y axis

        plt.savefig(os.path.join(output_dir, f'sas_imagery_{i}.png'))
        plt.close(fig)

def plot_relative_metrics(run_ids, metric_name="test_acc", plot_title="Percentage Difference in Accuracy Relative to ResNet18 1%", wandb_project="bsheffield2008/ssl-sas/"):
    api = wandb.Api()

    runs = [api.run(f"{wandb_project}{run_id}") for run_id in run_ids]

    # Create a dictionary with run ids and corresponding values
    data = {run.display_name: run.summary[metric_name] for run in runs}

    # rename
    data = {k.replace('resnet', 'ResNet'): v for k, v in data.items()}
    data = {k.replace('moco', 'SSL'): v for k, v in data.items()}
    data = {k.replace('r18', 'R18'): v for k, v in data.items()}
    data = {k.replace('-1chan', ''): v for k, v in data.items()}
    data = {k.replace(' SVM', ''): v for k, v in data.items()}
    data = {k.replace('.0', ''): v for k, v in data.items()}
    data = {k.replace('trial_24_', ''): v for k, v in data.items()}
    data = {k.replace('trial_26_', ''): v for k, v in data.items()}
    data = {k.replace('trial_28_', ''): v for k, v in data.items()}
    data = {k.replace('SVM', 'SSL'): v for k, v in data.items()}
    data = {k.replace('SSL_', 'SSL '): v for k, v in data.items()}

    data.update()

    # Convert the dictionary to a pandas Series
    s = pd.Series(data)

    # Compute the percentage difference with respect to the first run
    base_value = s[0]  # first run accuracy
    percentage_difference = ((s - base_value) / base_value) * 100

    # Create a bar chart for each run
    fig = go.Figure()

    # Add a bar for each percentage difference, excluding the first run
    for display_name, difference in percentage_difference.items():
        if display_name != list(percentage_difference.keys())[0]:  # Exclude first run
            fig.add_trace(go.Bar(
                x=[display_name],
                y=[difference],
                name=display_name
            ))

    fig.update_layout(
        title_text=plot_title,
        title_font=dict(size=24),
        # xaxis=dict(title="Model", titlefont=dict(size=20)),
        legend=dict(font=dict(size=20)),
        xaxis=dict(titlefont=dict(size=22), tickfont=dict(size=20)),
        yaxis=dict(titlefont=dict(size=22), tickfont=dict(size=20)),
        # paper_bgcolor = 'rgb(70, 70, 70)',  # Change this to your desired color
        # plot_bgcolor = 'rgb(70, 70, 70)',  # Change this to your desired color
    )

    # fig.update_xaxes(visible=False)

    fig.show()
    fig.write_image("/home/bsheffield/Pictures/oceans/" + metric_name + ".png")


def plot_overlay_curves(run_ids, path="/home/bsheffield/Pictures/oceans/", wandb_project="bsheffield2008/ssl-sas/"):

    api = wandb.Api()
    line_styles = ['dot', 'dash', 'solid'] * 3
    """
    Red: 'rgb(255,0,0)'
    Blue: 'rgb(0,0,255)'
    Green: 'rgb(0,255,0)'
    Purple: 'rgb(128,0,128)'
    Orange: 'rgb(255,165,0)'
    Cyan: 'rgb(0,255,255)'
    """
    color_palette = ['rgb(255,0,0)', 'rgb(0,0,255)', 'rgb(0,255,0)', 'rgb(128,0,128)', 'rgb(255,165,0)',
                     'rgb(0,255,255)']

    #runs = [api.run(f"bsheffield2008/ssl-sas-oceans/{run_id}") for run_id in run_ids]
    runs = [api.run(f"{wandb_project}/{run_id}") for run_id in run_ids]

    roc_fig = go.Figure()
    pr_fig = go.Figure()

    for i, run in enumerate(runs):
        # Download the ROC file
        roc_file = run.file('roc_data.npz')
        roc_file.download(replace=True)

        # Download the PR file
        pr_file = run.file('pr_data.npz')
        pr_file.download(replace=True)

        # Load the ROC data from the file
        roc_data = np.load('roc_data.npz')
        fpr = roc_data['fpr']
        tpr = roc_data['tpr']

        # Load the PR data from the file
        pr_data = np.load('pr_data.npz')
        precision = pr_data['precision']
        recall = pr_data['recall']

        # Get the line style for this run
        line_style = line_styles[i]

        # Rename the run display name
        run.display_name = run.display_name.replace('resnet', 'ResNet')
        run.display_name = run.display_name.replace('moco', 'SSL')
        run.display_name = run.display_name.replace('r18', 'R18')
        run.display_name = run.display_name.replace('-1chan', '')
        run.display_name = run.display_name.replace(' SVM', '')
        run.display_name = run.display_name.replace('.0', '')
        run.display_name = run.display_name.replace('trial_24_', '')
        run.display_name = run.display_name.replace('trial_26_', '')
        run.display_name = run.display_name.replace('trial_28_', '')
        run.display_name = run.display_name.replace('SVM', 'SSL')
        run.display_name = run.display_name.replace('SSL_', 'SSL ')

        # Get the color for this run
        line_color = color_palette[i]

        # Add to the ROC plot
        roc_fig.add_trace(
            go.Scatter(x=fpr, y=tpr, mode='lines', name=run.display_name, line=dict(color=line_color, dash=line_style))
        )

        # Add to the PR plot
        pr_fig.add_trace(
            go.Scatter(x=recall, y=precision, mode='lines', name=run.display_name,
                       line=dict(color=line_color, dash=line_style))
        )

    # Update ROC plot sizing
    roc_fig.update_layout(
        width=800,
        height=800,
        #paper_bgcolor='rgb(70, 70, 70)',  # Change this to your desired color
        #plot_bgcolor='rgb(70, 70, 70)',  # Change this to your desired color
        autosize=False,
        title_font=dict(size=24),  # Increase title font size
        title_text="ROC Curves",
        xaxis_title='False Positive Rate',
        yaxis_title='True Positive Rate',
        xaxis=dict(titlefont=dict(size=24), tickfont=dict(size=22)),
        yaxis=dict(titlefont=dict(size=24), tickfont=dict(size=22)),
        legend=dict(font=dict(size=24))
    )

    pr_fig.update_layout(
        width=800,
        height=800,
        autosize=False,
        #paper_bgcolor='rgb(70, 70, 70)',  # Change this to your desired color
        #plot_bgcolor='rgb(70, 70, 70)',  # Change this to your desired color
        title_font=dict(size=24),  # Increase title font size
        title_text="Precision-Recall Curves",
        xaxis_title='Recall',
        yaxis_title='Precision',
        xaxis=dict(titlefont=dict(size=24), tickfont=dict(size=22)),
        yaxis=dict(titlefont=dict(size=24), tickfont=dict(size=22)),
        legend=dict(font=dict(size=24)),
    )

    # Add diagonal 45 degree line to ROC plot
    roc_fig.add_shape(
        type='line',
        x0=0, y0=0, x1=1, y1=1,
        line=dict(color='rgba(0,0,0,0.5)', width=2, dash='dash'),
    )

    roc_fig.show()
    pr_fig.show()
    roc_fig.write_image(path + "roc.png")
    pr_fig.write_image(path + "pr.png")



def combine_images(image1, image2, output_name, titles=None, title_font_size=24):
    """Place images side-by-side"""

    if titles is None:
        titles = ["", ""]

    image1_trace = go.Image(z=image1)
    image2_trace = go.Image(z=image2)

    fig = make_subplots(rows=1, cols=2, subplot_titles=(titles[0], titles[1]))

    fig.add_trace(image1_trace, row=1, col=1)
    fig.add_trace(image2_trace, row=1, col=2)

    fig.update_xaxes(visible=False)
    fig.update_yaxes(visible=False)

    # update title font size
    #fig.update_annotations(dict(font_size=title_font_size))

    fig.show()
    fig.write_image(output_name)


def plot_augmentations():
    prob = 1.0
    sasdataset = SASDataset(hdf5_file_path="/mnt/data/hd2/sas/marc/test_balanced.h5")
    sas_img, label = sasdataset[93]
    sas_tf = SASDataAugmentation()
    sas_tf_img = sas_tf(sas_img).squeeze()
    sas_imgs = [
        sas_img,
        KA.RandomHorizontalFlip(p=prob)(sas_img).squeeze(),
        KA.RandomRotation(degrees=(-5, 5), p=prob)(sas_img).squeeze(),
        KA.RandomAffine(degrees=5, p=prob, translate=(0.1, 0.1), scale=(0.9, 1.1))(sas_img).squeeze(),
        KA.RandomResizedCrop(size=(224, 224), scale=(0.6, 1.), p=prob)(sas_img).squeeze(),
        KA.RandomGaussianBlur(kernel_size=(3, 3), sigma=(0.1, 2.0), p=prob)(sas_img).squeeze(),
        SpeckleNoise(noise_factor=0.1, p=prob)(sas_img).squeeze(),
        sas_tf_img
    ]

    aug_names = ["Original", "Horizontal Flip", "Rotation", "Affine", "Resized Crop", "Gaussian Blur", "Speckle Noise", "All"]
    cols = 4

    # Exclude the original image from augmentations and titles
    sas_imgs = sas_imgs[0:]
    aug_names = aug_names[0:]
    rows = int(np.ceil(len(sas_imgs) / cols))
    fig = make_subplots(rows=rows, cols=cols, subplot_titles=aug_names)

    for i, sas_img in enumerate(sas_imgs):
        row = i // cols + 1  # Compute row index
        col = i % cols + 1  # Compute column index
        high_freq_img = sas_img[1].numpy()
        high_freq_img = high_freq_img.transpose()
        high_freq_heatmap = go.Heatmap(z=high_freq_img, showscale=False, colorscale='Viridis')
        fig.add_trace(high_freq_heatmap, row=row, col=col)

    fig.update_layout(
        height=400 * rows,
        width=400 * cols,
        title_text="",
        annotations=[
            dict(
                #x=0.5,
                #y=0.5,
                #xref="paper",
                #yref="paper",
                #text="Main title",
                showarrow=False,
                font=dict(size=28)
            )
        ]
    )
    fig.update_xaxes(showticklabels=False, zeroline=False, showgrid=False, showline=False)
    fig.update_yaxes(showticklabels=False, zeroline=False, showgrid=False, showline=False)

    fig.show()
    fig.write_image("/home/bsheffield/Pictures/ssl-sas/" + "SASAugmentations.png")


def fourier_analysis(file_1):
    import matplotlib.pyplot as plt
    from scipy.fftpack import fft2, fftshift, ifftshift

    # Select a random sample from each dataset
    data_bb_sample = file_1['data_bb'][10, :, :, 0]  # 10 is a random index, use any index from 0 to 475
    data_hf_sample = file_1['data_hf'][10, :, :, 0]  # 10 is a random index, use any index from 0 to 475

    # Compute the 2D Fourier Transform of the sample
    fft_data_bb = fftshift(fft2(data_bb_sample))
    fft_data_hf = fftshift(fft2(data_hf_sample))

    # Compute the magnitude spectrum
    mag_spectrum_bb = np.abs(fft_data_bb)
    mag_spectrum_hf = np.abs(fft_data_hf)

    # Identify the dominant frequency
    dominant_frequency_bb = np.unravel_index(np.argmax(mag_spectrum_bb), mag_spectrum_bb.shape)
    dominant_frequency_hf = np.unravel_index(np.argmax(mag_spectrum_hf), mag_spectrum_hf.shape)

    # Create plots
    fig, ax = plt.subplots(2, 2, figsize=(12, 12))

    # Plot the original data_bb and data_hf samples
    ax[0, 0].imshow(np.abs(data_bb_sample), cmap='gray')
    ax[0, 0].set_title('Original Low Frequency Data')
    ax[0, 1].imshow(np.abs(data_hf_sample), cmap='gray')
    ax[0, 1].set_title('Original High Frequency Data')

    # Plot the magnitude spectrum of data_bb and data_hf
    ax[1, 0].imshow(np.log1p(mag_spectrum_bb), cmap='gray', extent=(-160, 160, -160, 160))
    ax[1, 0].plot(dominant_frequency_bb[1] - 160, dominant_frequency_bb[0] - 160, 'r+')
    ax[1, 0].set_title('Magnitude Spectrum of Low Frequency Data (Log Scale)')
    ax[1, 1].imshow(np.log1p(mag_spectrum_hf), cmap='gray', extent=(-160, 160, -160, 160))
    ax[1, 1].plot(dominant_frequency_hf[1] - 160, dominant_frequency_hf[0] - 160, 'r+')
    ax[1, 1].set_title('Magnitude Spectrum of High Frequency Data (Log Scale)')

    plt.tight_layout()
    plt.show()

    dominant_frequency_bb, dominant_frequency_hf



def geographic_distribution(filename):
    import folium
    import numpy as np
    import h5py
    with h5py.File(filename, 'r') as file_1:
        # Extract the contact coordinates
        contact_coords = file_1['contact_xy'][()]

        # Split into x (longitude) and y (latitude) coordinates
        x_coords = contact_coords[:, 0]
        y_coords = contact_coords[:, 1]

        # Initialize map centered around average coordinates
        m = folium.Map(location=[np.mean(y_coords), np.mean(x_coords)], zoom_start=2)

        # Add points to map
        for x, y in zip(x_coords, y_coords):
            folium.CircleMarker([y, x], radius=5, color='blue', fill=True, fill_color='blue', fill_opacity=0.6).add_to(m)

        # Save map to HTML file
        m.save('map.html')

        import webbrowser
        import os

        webbrowser.open('file://' + os.path.realpath('map.html'))


def feature_extraction_complex(file_1):
    from scipy.stats import skew, kurtosis
    from scipy.fftpack import fft2
    from scipy.signal import welch

    # Function to extract features from a sample
    def extract_features(sample):
        # Compute statistical features
        mean = np.mean(sample)
        std_dev = np.std(sample)
        skewness = skew(sample.flatten())
        kurtosis_val = kurtosis(sample.flatten())

        # Compute spectral features
        # Compute the 2D Fourier Transform of the sample
        f, Pxx_den = welch(sample.flatten(), nperseg=1024)
        mean_psd = np.mean(Pxx_den)

        return [mean, std_dev, skewness, kurtosis_val, mean_psd]

    # Extract features from a sample in each dataset
    features_bb = extract_features(file_1['data_bb'][10, :, :, 0])  # 10 is a random index, use any index from 0 to 475
    features_hf = extract_features(file_1['data_hf'][10, :, :, 0])  # 10 is a random index, use any index from 0 to 475

    # Display the features
    features_bb, features_hf


# Function to extract features from a sample, focusing on magnitude
def extract_features_magnitude(sample):
    """
    Example usage:

    # Extract features from a sample in each dataset
    features_bb_mag = extract_features_magnitude(file_1['data_bb'][10, :, :, 0])  # 10 is a random index, use any index from 0 to 475
    features_hf_mag = extract_features_magnitude(file_1['data_hf'][10, :, :, 0])  # 10 is a random index, use any index from 0 to 475

    # Display the features
    features_bb_mag, features_hf_mag
    """

    # Compute statistical features on the magnitude of the sample
    magnitude = np.abs(sample)
    mean = np.mean(magnitude)
    std_dev = np.std(magnitude)
    skewness = skew(magnitude.flatten())
    kurtosis_val = kurtosis(magnitude.flatten())

    # Compute spectral features on the magnitude of the sample
    f, Pxx_den = welch(magnitude.flatten(), nperseg=1024)
    mean_psd = np.mean(Pxx_den)

    return [mean, std_dev, skewness, kurtosis_val, mean_psd]


# Function to extract features from a sample, focusing on phase
def extract_features_phase(sample):

    # Compute statistical features on the phase of the sample
    phase = np.angle(sample)
    mean = np.mean(phase)
    std_dev = np.std(phase)
    skewness = skew(phase.flatten())
    kurtosis_val = kurtosis(phase.flatten())

    # Compute spectral features on the phase of the sample
    f, Pxx_den = welch(phase.flatten(), nperseg=1024)
    mean_psd = np.mean(Pxx_den)

    return [mean, std_dev, skewness, kurtosis_val, mean_psd]

def print_aggregated_metrics(results):
    import numpy as np

    metrics = ['Test Accuracy', 'Test Precision', 'Test Recall', 'Test F1']
    for metric in metrics:
        values = [result[metric] for result in results]
        mean = np.mean(values)
        std = np.std(values)
        print(f'{metric}: Mean = {mean:.2f}, Std Dev = {std:.2f}')

def get_performance_tier(rank, total):
    if rank <= total // 3:
        return 'High'
    elif rank <= 2 * total // 3:
        return 'Medium'
    else:
        return 'Low'

def print_latex_table(results):
    print('\\begin{table}[h]')
    print('\\begin{tabular}{|c|c|c|c|}')
    print('\\hline')
    print('Rank & Backbone & Label \% & Num Channels & Performance Tier \\\\ \\hline')
    for i, result in enumerate(results, start=1):
        print(f"{i} & {result['Backbone']} & {result['Label %']} & {result['Num Channels']} & {get_performance_tier(i, len(results))} \\\\ \\hline")
    print('\\end{tabular}')
    print('\\caption{Downstream classification models ranked by Performance Tier}')
    print('\\label{tab:model_ranking}')
    print('\\end{table}')

def summarize_results(wandb_project_name="ssl-sas"):
    import wandb

    api = wandb.Api()
    runs = api.runs(wandb_project_name)
    results = []

    for run in runs:
        if "trial_" not in run.name and "SVM" not in run.name:
            continue
        #if "100.0%" not in run.name and "1chan" not in run.name:
        #    continue
        #if run.config["num_channels"] != 1:
        #    continue
        #if run.config["label_percentage"] != 1 and run.config["label_percentage"] != 5 and run.config["label_percentage"] != 10 and run.config["label_percentage"] != 100:
        if run.config["label_percentage"] != 1 and run.config["label_percentage"] != 10 and run.config["label_percentage"] != 100:
            continue
        summary = run.summary
        row = {
            "Run ID": run.id,
            "Classifier": run.config["classifier_name"],
            "Num Channels": run.config["num_channels"],
            "Backbone": run.config["backbone_name"],
            "Label %": run.config["label_percentage"],
            "Run Name": run.name,
            "Test Accuracy": summary["test_acc"],
            "Test Precision": summary["test_precision"],
            "Test Recall": summary["test_recall"],
            "Test F1": summary["test_f1"],
        }
        results.append(row)

    # Sort the list of results by Test F1 in descending order
    results = sorted(results, key=lambda x: x['Test F1'], reverse=True)

    print("F1-Score Ranked")
    for result in results:
        print(result)

    print_latex_table(results)

    #print_aggregated_metrics(results)



if __name__ == '__main__':

    run_ids = \
    [
        #"9q25r3v8", "lsowcpn1", "amcaf7hk", # resnet18 1%, 10%, 100%
        "o9riveg1","n6nth3yw","5umgx55u",
        #"idbn03h6", "czcbkmnf", "0oum82q3", # moco r18 SVM 1%, 10%, 100%
        "lz3xgllr", "kunazkpp", "7aeg9nf7",
    ]

    #plot_relative_metrics(run_ids, metric_name="test_acc", plot_title="Percentage Difference in Accuracy Relative to ResNet18 1%")
    plot_relative_metrics(run_ids, metric_name="test_f1",  plot_title="Percentage Difference in F1 Score Relative to ResNet18 1%")
    #plot_relative_metrics(run_ids, metric_name="test_precision", plot_title="Percentage Difference in Precision Relative to ResNet18 1%")
    #plot_relative_metrics(run_ids, metric_name="test_recall", plot_title="Percentage Difference in Recall Relative to ResNet18 1%")
    plot_overlay_curves(run_ids, path="/home/sheffield/Pictures/", wandb_project="bsheffield2008/ssl-sas/")

    summarize_results("ssl-sas")

    #geographic_distribution("/mnt/data/hd2/sas/marc/test_balanced.h5")
    #plot_augmentations()
    '''path = "/home/bsheffield/Pictures/oceans/"
    roc_img = Image.open(path+"sas_imagery_406.png")
    pr_img = Image.open(path+"sas_tf_imagery_406.png")
    combine_images(roc_img, pr_img, "/home/bsheffield/Pictures/oceans/combined-augs.png")'''