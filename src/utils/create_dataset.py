import argparse
import glob
import os
from multiprocessing import Pool

import pandas as pd
import h5py
import numpy as np
from concurrent.futures import ProcessPoolExecutor

"""
Create unified dataset from multiple processed files.
"""

def find_files(directory, suffix):
    return glob.glob(f"{directory}/**/*{suffix}", recursive=True)


def process_file(file_chunk, output_file, sensor_name, HF="HF", BB="BB"):
    try:
        with h5py.File(output_file, 'w') as hf_out:
            hf_datas = []
            bb_datas = []
            dformats = []
            gts = []
            ids = []
            fns = []
            detectors = []
            normalizers = []
            sides = []
            uuids = []
            clss = []
            x_coords = []
            y_coords = []
            types = []
            for file_name in file_chunk:
                try:
                    with h5py.File(file_name, 'r') as hf:
                        if str(hf['/sensor'][0].decode()) != sensor_name:
                            continue
                        for subgroup in hf['/ecdata']:
                            if not hf.get(f'/ecdata/{subgroup}/data/{HF}/snippet/'):
                                continue
                            hf_data_path = f'/ecdata/{subgroup}/data/{HF}/snippet/data_real'
                            bb_data_path = f'/ecdata/{subgroup}/data/{BB}/snippet/data_real'
                            dformat_path = f'/ecdata/{subgroup}/data/{HF}/snippet/data_format'
                            contact_path = f'/contacts/{subgroup}/'
                            if hf_data_path in hf and bb_data_path in hf and contact_path in hf:
                                bb_datas.append(np.array(hf[bb_data_path]))
                                hf_datas.append(np.array(hf[hf_data_path]))
                                dformats.append(int(hf[dformat_path][0]))
                                gt = hf[contact_path + 'gt'][:]
                                id = hf[contact_path + 'id'][:]
                                fn = hf[contact_path + 'fn'][:]
                                det = hf[contact_path + 'detector'][:]
                                norm = hf[contact_path + 'normalizer'][:]
                                side = hf[contact_path + 'side'][:]
                                uid = hf[contact_path + 'uuid'][:]
                                cls = hf[contact_path + 'cls'][:]
                                type = hf[contact_path + 'type'][:]
                                x = hf[contact_path + 'x'][:]
                                y = hf[contact_path + 'y'][:]

                                gts.append(gt)
                                ids.append(id)
                                fns.append(fn)
                                detectors.append(det)
                                normalizers.append(norm)
                                sides.append(side)
                                uuids.append(uid)
                                clss.append(cls)
                                types.append(type)
                                x_coords.append(x)
                                y_coords.append(y)

                except Exception as e:
                    print(f'Error processing file {file_name}: {e}')

            hf_out.create_dataset('hf', data=hf_datas)
            hf_out.create_dataset('bb', data=bb_datas)
            hf_out.create_dataset('dformat', data=dformats)
            hf_out.create_dataset('gt', data=gts)
            hf_out.create_dataset('id', data=ids)
            hf_out.create_dataset('fn', data=fns)
            hf_out.create_dataset('detector', data=detectors)
            hf_out.create_dataset('normalizer', data=normalizers)
            hf_out.create_dataset('side', data=sides)
            hf_out.create_dataset('uuid', data=uuids)
            hf_out.create_dataset('cls', data=clss)
            hf_out.create_dataset('type', data=types)
            hf_out.create_dataset('x', data=x_coords)
            hf_out.create_dataset('y', data=y_coords)
    except Exception as e:
        print(f'Error creating output file: {e}')

def merge_h5_files(file_list, output_file='ssam2-contacts.h5', sensor_name="ONR SSAM2"):
    num_split = 32
    chunk_size = len(file_list) // num_split
    file_chunks = [file_list[i:i + chunk_size] for i in range(0, len(file_list), chunk_size)]

    with Pool(num_split) as pool:
        # Use starmap to apply the process_file function to each file in file_list.
        # starmap is similar to map, but allows multiple arguments to be passed to the worker function.
        pool.starmap(process_file, [(file_chunk, f"{output_file}_{i}.h5", sensor_name) for i, file_chunk in enumerate(file_chunks)])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('output', type=str, help='Full path output file e.g. /tmp/output.h5')
    parser.add_argument('sensor_name', type=str, help='Exact sensor name to create a dataset for.')
    args = parser.parse_args()
    output = args.output
    sensor_name = args.sensor_name

    #h5_files = find_files("/media/Tank/ET-850-Training/", ".h5")
    h5_files = find_files("/media/Tank/PerfReport", ".h5")
    trinity_files = find_files("/media/Trinity/PerfReport", ".h5")
    h5_files.extend(trinity_files)
    print(len(h5_files))
    if sensor_name == 'SSAM2':
        sensor_name = 'ONR SSAM2'
    merge_h5_files(h5_files, output, sensor_name=sensor_name)
    process_file(h5_files, output, sensor_name=sensor_name)
