"""
This module contains the SASDataModule, a custom data module for handling the SAS dataset.
It provides structures for efficient data loading, batching, and preprocessing within the PyTorch Lightning framework.
"""

import pytorch_lightning as pl
import torch
from torch.utils.data import DataLoader

from src.datasets.SASDataset import SASDataset, get_subset_dataset


class SASDataModule(pl.LightningDataModule):
    def __init__(self, train_file, val_file=None, test_file=None, only_gt=False, num_channels=2, train_bs=256,
                 val_bs=256, test_bs=256, num_workers=8, label_percentage=None, combine_test=False, transforms=None,
                 shuffle=False, binary=True):
        """
        Initialize the SASDataModule with the specified parameters.

        Parameters:
        - train_file (str): Path to the training HDF5 file.
        - val_file (str, optional): Path to the validation HDF5 file. Default is None.
        - test_file (str, optional): Path to the testing HDF5 file. Default is None.
        ...

        Notes:
        - Initializes dataset references, file paths, and various other configuration parameters.
        """

        super().__init__()
        self.transform = None
        self.train_dataset = self.val_dataset = self.test_dataset = None
        self.train_file, self.val_file, self.test_file = train_file, val_file, test_file
        self.only_gt, self.train_bs, self.num_workers, self.val_bs, self.test_bs = only_gt, train_bs, num_workers, \
            val_bs, test_bs
        self.transforms = transforms
        self.label_percentage = label_percentage
        self.num_channels = num_channels
        self.shuffle = shuffle
        self.binary = binary
        self.combine_test = combine_test


    def setup(self, stage=None):
        """
        Setup datasets for different stages (train, val, test) based on the given stage.

        Parameters:
        - stage (str, optional): Current stage (either 'fit' or 'test'). Default is None which considers both.

        Notes:
        - Initializes the training dataset and optionally the validation dataset during the 'fit' stage.
        - Initializes the testing dataset during the 'test' stage. If combine_test is True, it combines the validation and testing datasets.
        """

        if stage == 'fit' or stage is None:
            self.train_dataset = SASDataset(self.train_file, num_channels=self.num_channels, only_gt=self.only_gt,
                                            binary=self.binary, transforms=self.transforms)
            if self.val_file is not None:
                self.val_dataset = SASDataset(self.val_file, only_gt=self.only_gt, binary=self.binary, num_channels=self.num_channels)
            self.train_dataset = get_subset_dataset(self.train_dataset, percentage=self.label_percentage)
        if stage == 'test' or stage is None:
            if self.test_file is not None:
                self.test_dataset = SASDataset(self.test_file, only_gt=self.only_gt, binary=self.binary, num_channels=self.num_channels)
                if self.combine_test:
                    self.test_dataset = torch.utils.data.ConcatDataset([self.val_dataset, self.test_dataset])

    def train_dataloader(self):
        """
        Create and return a DataLoader for the training dataset.

        Returns:
        - DataLoader: DataLoader for the training dataset with the specified configurations.
        """

        return DataLoader(self.train_dataset, batch_size=self.train_bs, shuffle=self.shuffle,
                          num_workers=self.num_workers,
                          pin_memory=True, drop_last=True)

    def val_dataloader(self):
        """
        Create and return a DataLoader for the validation dataset.

        Returns:
        - DataLoader: DataLoader for the validation dataset with the specified configurations.
        """

        return DataLoader(self.val_dataset, batch_size=self.val_bs, shuffle=False, num_workers=self.num_workers,
                          pin_memory=True, drop_last=True)

    def test_dataloader(self):
        """
        Create and return a DataLoader for the testing dataset.

        Returns:
        - DataLoader: DataLoader for the testing dataset with the specified configurations.
        """

        return DataLoader(self.test_dataset, batch_size=self.test_bs, shuffle=False, num_workers=self.num_workers,
                          pin_memory=True, drop_last=True)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='SAS DataModule')
    parser.add_argument('--train_data', metavar='DIR', help='path to H5 dataset file')
    parser.add_argument('--val_data', type=str, help='path to H5 validation dataset')
    parser.add_argument('--test_data', type=str, help='path to H5 test dataset')
    args = parser.parse_args()

    from src.datasets.SASAugmentations import SASDataAugmentation
    prob = 1.0
    sas_tf = SASDataAugmentation(apply_strong_augmentations=True, prob=prob)

    val_dataset = SASDataset(args.val_data, num_channels=1, binary=True, only_gt=False, transforms=sas_tf)
    from src.utils.plotting import save_sas_imagery_plots
    save_sas_imagery_plots(val_dataset, output_dir="sas_val_tf_images", cmap_color="pink")
