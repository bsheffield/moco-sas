"""
This module contains the SASDataset class tailored for handling data in the HDF5 file format.
It provides methods for reading, preprocessing, and serving the data to a PyTorch model.
"""

import h5py
import kornia as K
import numpy as np
import random
import torch
from torch.utils.data import Dataset, Subset


class SASDataset(Dataset):
    def __init__(self, hdf5_file_path: str, num_channels: int = 2, only_gt: bool = False,
                 target_shape=(224, 224), binary: bool = True, transforms=None):
        """
        Initialize the SASDataset with the specified parameters.

        Parameters:
        - hdf5_file_path (str): Path to the HDF5 file containing the dataset.
        - num_channels (int, optional): Number of input channels. Default is 2.
        - only_gt (bool, optional): If True, only labels with a value of 1 are kept. Default is False.
        - target_shape (tuple, optional): Desired shape of the dataset items. Default is (224, 224).
        - binary (bool, optional): If True, the dataset is considered binary. Default is True.
        - transforms (callable, optional): Optional transformations to be applied on the dataset items. Default is None.

        Notes:
        1. Loads labels and target types from the HDF5 file.
        2. If `only_gt` is True, filters labels to only keep those with a value of 1.
        3. If `binary` is False, further processes the target types to filter out zeros and adjust the labeling.
        """

        super().__init__()
        self.hdf5_file_path = hdf5_file_path
        self.transforms = transforms
        self.target_shape = target_shape
        self.binary = binary
        self.num_channels = num_channels

        with h5py.File(hdf5_file_path, 'r') as file:
            self.labels, self.target_type = file['/labels'][:], file['/target_type'][:]
            if only_gt:
                # only keep labels with 1 value with corresponding target type
                self.target_type = self.target_type[self.labels == 1]
                self.labels = self.labels[self.labels == 1]

            if not self.binary:
                # Filter out target_type values of 0
                non_zero_indices = np.where(self.target_type > 0)
                self.labels = self.labels[non_zero_indices]
                self.target_type = self.target_type[non_zero_indices]

                # TorchMetrics expects labels to start at 0. Shift target_type from [1, 2, 3] to [0, 1, 2]
                self.target_type -= 1

        self.has_second_channel = self.num_channels != 1

    def __len__(self):
        """
        Get the total number of samples in the dataset.

        Returns:
        - int: Total number of samples.
        """
        return len(self.target_type)

    def __getitem__(self, index):
        """
        Retrieve a sample from the dataset given an index.

        Parameters:
        - index (int): Index of the desired sample.

        Returns:
        - tuple: A tuple containing the sample image (torch.Tensor) and its corresponding label (int).

        Notes:
        - The method reads the data from the HDF5 file for the given index.
        - Data is preprocessed and optionally transformed.
        - If the dataset is binary, labels are either 0 or 1. Otherwise, labels correspond to the target type.
        - In case of errors, the method returns None for both the image and label.
        """

        with h5py.File(self.hdf5_file_path, 'r') as file:
            try:
                data_hf = file['/data_hf'][index, :, :, 0]
                data_hf_normalized = self.preprocess_data(data_hf)
                data_hf_tensor = torch.from_numpy(data_hf_normalized)
                resized_data_hf_tensor = K.geometry.transform.resize(data_hf_tensor.unsqueeze(0),
                                                                     self.target_shape).squeeze(0)

                if self.has_second_channel:
                    data_bb = file['/data_bb'][index, :, :, 0]
                    data_bb_normalized = self.preprocess_data(data_bb)
                    data_bb_tensor = torch.from_numpy(data_bb_normalized)
                    resized_data_bb_tensor = K.geometry.transform.resize(data_bb_tensor.unsqueeze(0),
                                                                         self.target_shape).squeeze(0)
                    sas_img = torch.stack((resized_data_bb_tensor, resized_data_hf_tensor))
                else:
                    sas_img = resized_data_hf_tensor.unsqueeze(0)

                if self.transforms:
                    sas_img = self.transforms(sas_img)

                if self.binary:
                    label = 1 if self.labels[index] == 1 else 0
                else:
                    label = self.target_type[index]

                return sas_img, label

            except Exception as e:
                print(f"Error loading data at index {index}: {e}")
                return None, None

    @staticmethod
    def preprocess_data(data, clip_percentiles=(1, 99)):
        """
        Preprocess the given data using normalization and optional percentile clipping.

        Parameters:
        - data (numpy.ndarray): Input data array to be preprocessed.
        - clip_percentiles (tuple, optional): Percentiles for clipping the data. Default is (1, 99).

        Returns:
        - numpy.ndarray: Preprocessed data.

        Notes:
        - This method applies normalization to the data using the scale function.
        """

        data_normalized = scale(data)
        return data_normalized


def scale(data):
    """
    Normalize the given data by its mean magnitude and clip values at 16.

    Parameters:
    - data (numpy.ndarray): Input complex data array to be normalized.

    Returns:
    - numpy.ndarray: Normalized data.

    Notes:
    - This function first computes the magnitude representation of the complex data.
    - Data is then normalized by dividing by its mean magnitude.
    - Values are clipped at 16 to limit the range.
    """


def normalize(data):
    """
    normalize: A method/function to ... 
    """
    """
        ---Notes---
        -The choice of 16 as the upper limit for clipping is somewhat arbitrary. It might be better to choose this
          value based on the characteristics of your data. For instance, you might want to set it to a value that
          excludes a certain percentage of the most extreme values.
        -This method does not standardize the data to have a mean of 0 and a standard deviation of 1, which is a common
          requirement for many machine learning algorithms. If the algorithm you're using expects this, you might need
          to adjust your normalization approach.
        -Dividing by the mean can be sensitive to extreme values, since they can have a large effect on the mean. If
          your data contains outliers, this might distort the normalization.
    """

    data_magnitude = np.abs(data)  # transform the complex data into a magnitude representation
    data_normalized = data_magnitude / np.mean(data_magnitude)
    data_normalized = np.clip(data_normalized, a_min=None, a_max=16)  # Clip values at 16

    return data_normalized


def get_subset_dataset(dataset: object, percentage: float) -> object:
    """
    Get a subset of the given dataset based on the specified percentage.

    Parameters:
    - dataset (object): Input dataset from which a subset is to be extracted.
    - percentage (float): Percentage of the dataset to be included in the subset (0 <= percentage <= 1).

    Returns:
    - object: Subset of the original dataset.

    Notes:
    - Randomly samples a subset of indices based on the specified percentage.
    - Creates a new dataset using the Subset class from PyTorch with the sampled indices.
    """

    num_samples = len(dataset)
    num_subset_samples = int(num_samples * percentage)

    # Generate a list of random indices
    random_indices = random.sample(range(num_samples), num_subset_samples)

    # Create a new dataset with the subset of data
    subset_dataset = Subset(dataset, random_indices)

    return subset_dataset

if __name__ == '__main__':
    import argparse
    from src.utils.plotting import save_sas_imagery_plots

    parser = argparse.ArgumentParser(description='SAS DataModule')
    parser.add_argument('--train_data', metavar='DIR', help='path to H5 dataset file')
    parser.add_argument('--val_data', type=str, help='path to H5 validation dataset')
    parser.add_argument('--test_data', type=str, help='path to H5 test dataset')
    args = parser.parse_args()

    val_dataset = SASDataset(args.val_data, num_channels=1, binary=True, only_gt=False)
    save_sas_imagery_plots(val_dataset, output_dir="sas_val_images", cmap_color="pink")
