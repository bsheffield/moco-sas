"""
This module contains various data augmentation techniques tailored for the SAS dataset.
It provides classes and methods to apply specific augmentations like Rayleigh noise.
"""

import random
import torch
from PIL.Image import Image
from kornia import augmentation as KA
from torch import Tensor
from typing import List, Union


class RayleighNoise(torch.nn.Module):
    """
    Implements the Rayleigh noise augmentation for the SAS dataset.

    Attributes:
    - scale (float): Scale factor for the noise intensity.
    - p (float): Probability of applying the noise.

    Methods:
    - forward(x: torch.Tensor) -> torch.Tensor: Applies Rayleigh noise to the input tensor based on the set probability.
    """
    def __init__(self, scale=0.1, p=0.5):
        super().__init__()
        self.scale = scale
        self.p = p

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        probability = torch.rand(1).item()
        if probability > self.p:
            u = torch.randn_like(x)
            v = torch.randn_like(x)
            noise = self.scale * torch.sqrt(u ** 2 + v ** 2)
            return x + noise
        else:
            return x
class SpeckleNoise(torch.nn.Module):
    """
    Implements the speckle noise augmentation for the SAS dataset.

    Attributes:
    - noise_factor (float): Factor controlling the intensity of speckle noise.
    - p (float): Probability of applying the noise.

    Methods:
    - forward(x: torch.Tensor) -> torch.Tensor: Applies speckle noise to the input tensor based on the set probability.
    """
    def __init__(self, noise_factor=0.1, p=0.5):
        super().__init__()
        self.noise_factor = noise_factor
        self.p = p

    def forward(self, x: torch.Tensor) -> torch.Tensor:

        probability = random.random()
        if probability > self.p:
            noise = torch.randn_like(x) * self.noise_factor
            return x * noise
        else:
            return x

class SASDataAugmentation(torch.nn.Module):
    """
    Comprehensive data augmentation class tailored for the SAS dataset. It includes base and strong augmentations.

    Attributes:
    - apply_strong_augmentations (bool): Flag to determine if strong augmentations should be applied.
    - base_transforms (torch.nn.Sequential): Sequence of basic augmentations.
    - strong_transforms (torch.nn.Sequential): Sequence of strong augmentations.

    Methods:
    - forward(x: torch.Tensor) -> torch.Tensor: Applies the defined augmentations to the input tensor.
    """
    def __init__(self, apply_strong_augmentations=True, prob=0.5):
        super().__init__()
        self.apply_strong_augmentations = apply_strong_augmentations

        self.base_transforms = torch.nn.Sequential(
            KA.RandomHorizontalFlip(p=prob),
        )

        self.strong_transforms = torch.nn.Sequential(
            KA.RandomResizedCrop(size=(224, 224), scale=(0.6, 1.2), p=prob),
            KA.RandomRotation(degrees=(-5, 5), p=prob),
            KA.RandomAffine(degrees=5, p=prob, translate=(0.1, 0.1), scale=(0.9, 1.1)),
            RayleighNoise(scale=0.1, p=prob),
            SpeckleNoise(noise_factor=0.1, p=prob),
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.base_transforms(x)
        if self.apply_strong_augmentations:
            x = self.strong_transforms(x)
        x = x.squeeze(0)
        return x


class MultiViewTransform:
    """
    MultiViewTransform:: A class to ... 
    """
    """Transforms an image into multiple views.

    Args:
        transforms:
            A sequence of transforms. Every transform creates a new view.

    """

    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, image: Union[Tensor, Image]) -> Union[List[Tensor], List[Image]]:
        """Transforms an image into multiple views.

        Every transform in self.transforms creates a new view.

        Args:
            image:
                Image to be transformed into multiple views.

        Returns:
            List of views.

        """
        return [transform(image) for transform in self.transforms]

class SASMultiViewTransform(MultiViewTransform):
    """
    Multi-view transformation tailored for the SAS dataset using the SASDataAugmentation class.

    Attributes:
    - transforms (List[torch.nn.Module]): List of transformations to be applied for multi-view.

    Note: Inherits from the base `MultiViewTransform` class.
    """

    def __init__(self, strong_transforms=True):
        sas_transform = SASDataAugmentation(strong_transforms)
        super().__init__(transforms=[sas_transform, sas_transform])