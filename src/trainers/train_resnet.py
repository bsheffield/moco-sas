import warnings
from itertools import product
import pytorch_lightning as pl
import torch
import optuna
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
from pytorch_lightning.loggers.wandb import WandbLogger
from src.datasets.SASAugmentations import SASDataAugmentation
from src.datasets.SASDatamodule import SASDataModule
from pytorch_lightning.callbacks.progress import TQDMProgressBar
from src.models.resnet.resnet import ResNetClassifier
from src.utils.trainer_argparser import parser

class GridSampler(optuna.samplers.BaseSampler):
    """
    Custom sampler for hyperparameter optimization using Optuna.
    This sampler iterates over a predefined grid of hyperparameters.

    Attributes:
    - grid (dict): Dictionary containing the grid of hyperparameters.
    - grid_combinations (list): List of all possible combinations of hyperparameters.
    - param_names (list): List of parameter names in the grid.
    - current_trial (int): Index of the current trial in the grid.

    Methods:
    - infer_relative_search_space(study, trial) -> dict: Returns the predefined grid.
    - sample_relative(study, trial, search_space) -> dict: Samples the next set of hyperparameters from the grid.
    - sample_independent(study, trial, param_name, param_distribution): Raises an error since independent sampling is not implemented.
    """
    def __init__(self, grid):
        self.grid = grid
        # Convert the grid dictionary to a list of (name, values) pairs
        grid_items = list(grid.items())
        # Use itertools.product to generate all combinations
        self.grid_combinations = list(product(*[values for name, values in grid_items]))
        self.param_names = [name for name, values in grid_items]
        self.current_trial = 0

    def infer_relative_search_space(self, study, trial):
        return {name: optuna.distributions.CategoricalDistribution(values) for name, values in self.grid.items()}

    def sample_relative(self, study, trial, search_space):
        # Use the current combination of hyperparameters for this trial
        combination = self.grid_combinations[self.current_trial]
        self.current_trial += 1
        return {name: value for name, value in zip(self.param_names, combination)}

    def sample_independent(self, study, trial, param_name, param_distribution):
        # This method is used for parameters not contained in the search space.
        raise NotImplementedError


def train_resnet(trial):
    """
    Training function for the ResNet classifier using the SAS dataset and PyTorch Lightning.

    Parameters:
    - trial (optuna.Trial): Optuna trial object containing hyperparameters.

    Notes:
    - Sets up the model, data module, and trainer.
    - Trains and tests the model.
    - Logs results and metrics using Wandb.
    - Uses the provided hyperparameters from the trial for setting up the model and training configurations.
    """

    lr = trial.relative_params['lr']
    batch_size = trial.relative_params['batch_size']
    num_channels = trial.relative_params['num_channels']
    label_percentage = trial.relative_params['label_percentage']
    weight_decay = trial.relative_params['weight_decay']
    max_epochs = trial.relative_params['max_epochs']
    model_type = trial.relative_params['model']
    seed = trial.relative_params['seed']

    pl.seed_everything(seed)

    logger = WandbLogger(project="ssl-sas", log_model=True, name=str(model_type) + ": " +str(int(label_percentage * 100)) + "%")
    monitor_metric = "train_loss"
    objective_metric = "min"

    model = ResNetClassifier(weight_decay=weight_decay, arch=model_type, num_channels=num_channels, lr=lr, max_epochs=max_epochs)
    trainer = pl.Trainer(
        accelerator="auto",
        devices=1,
        logger=logger,
        fast_dev_run=args.fast_dev_run,
        max_epochs=max_epochs,
        callbacks=[TQDMProgressBar(refresh_rate=20),
                   ModelCheckpoint(save_weights_only=False, save_top_k=1, mode=objective_metric, monitor=monitor_metric,
                                   filename=model_type + "-{epoch:02d}-" + "{" + monitor_metric + ":.2f}"),
                   LearningRateMonitor("epoch"),
                   ]
    )
    sas_transform = SASDataAugmentation(apply_strong_augmentations=False)
    dm = SASDataModule(train_file=args.train_data, val_file=args.val_data, test_file=args.test_data,
                       train_bs=batch_size, val_bs=batch_size, test_bs=batch_size,
                       combine_test=True,
                       transforms=sas_transform, num_channels=num_channels,
                       binary=True, shuffle=True, label_percentage=label_percentage)
    try:
        trainer.fit(model, datamodule=dm)
        trainer.test(model, datamodule=dm)
    except Exception as e:
        print(str(e))
    finally:
        logger.experiment.finish()

if __name__ == '__main__':
    args = parser.parse_args()
    torch.set_float32_matmul_precision('high')
    warnings.filterwarnings("ignore")

    grid = {
        'lr': [0.0003],
        'batch_size': [64],
        'seed': [args.seed],
        'num_channels': [1, 2],
        'label_percentage': [0.01, 0.05, 0.1, 0.5, 1.0],
        'weight_decay': [0.0001],
        'max_epochs': [100],
        'model': ['resnet18']
    }

    # Count the number of trials to perform for Grid Search
    sampler = GridSampler(grid)
    study = optuna.create_study(sampler=sampler, direction='maximize')
    n_trials = 1
    for param_values in grid.values():
        n_trials *= len(param_values)

    study.optimize(train_resnet, n_trials=n_trials)