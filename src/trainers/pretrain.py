import optuna
import pytorch_lightning as pl
import torch
import warnings
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor, EarlyStopping
from pytorch_lightning.callbacks.progress import TQDMProgressBar
from pytorch_lightning.loggers.wandb import WandbLogger
from pytorch_lightning.strategies.ddp import DDPStrategy

from src.datasets.SASAugmentations import SASMultiViewTransform
from src.datasets.SASDatamodule import SASDataModule
from src.models.moco.mocov2 import MoCo
from src.utils.trainer_argparser import parser


class GridSampler(optuna.samplers.BaseSampler):
    def __init__(self, grid):
        """
        Custom sampler for hyperparameter optimization using Optuna.
        This sampler iterates over a predefined grid of hyperparameters.

        Attributes:
        - grid (dict): Dictionary containing the grid of hyperparameters.
        - current_trial (int): Index of the current trial in the grid.

        Methods:
        - infer_relative_search_space(study, trial) -> dict: Returns the predefined grid.
        - sample_relative(study, trial, search_space) -> dict: Samples the next set of hyperparameters from the grid.
        - sample_independent(study, trial, param_name, param_distribution): Raises an error as independent sampling is not implemented.
        """

        self.grid = grid
        self.current_trial = 0

    def infer_relative_search_space(self, study, trial):
        return self.grid

    def sample_relative(self, study, trial, search_space):
        values = {}
        for param_name, param_values in self.grid.items():
            values[param_name] = param_values[self.current_trial % len(param_values)]
        self.current_trial += 1
        return values

    def sample_independent(self, study, trial, param_name, param_distribution):
        # This method is used for parameters not contained in the search space.
        # You can return a default value or raise an error here.
        raise NotImplementedError

grid = {
    'lr': [0.3],
    'batch_size': [256],
    'num_channels': [1],
    'temperature': [0.07],
    'memory_bank': [4096],
    'warmup_epochs': [10],
    'momentum': [0.999],
    'weight_decay': [0.0001],
    'max_epochs': [100],
    'model': ['moco'],
    'backbone_type': ['resnet18', 'resnet34'],
}

def pretrain(trial):
    # Suggest values for the hyperparameters
    #lr = trial.suggest_loguniform('lr', 1e-5, 1.0)

    lr = trial.relative_params['lr']
    batch_size = trial.relative_params['batch_size']
    temperature = trial.relative_params['temperature']
    memory_bank = trial.relative_params['memory_bank']
    momentum = trial.relative_params['momentum']
    num_channels = trial.relative_params['num_channels']
    weight_decay = trial.relative_params['weight_decay']
    max_epochs = trial.relative_params['max_epochs']
    warmup_epochs = trial.relative_params['warmup_epochs']
    model_type = trial.relative_params['model']
    backbone_type = trial.relative_params['backbone_type']

    logger = WandbLogger(project="ssl-sas", name=model_type, log_model=True, job_type="pretrain")

    sas_transform = SASMultiViewTransform(strong_transforms=True)
    datamodule = SASDataModule(train_file=args.train_data, val_file=args.val_data, test_file=args.test_data,
                               num_channels=num_channels,
                               shuffle=True,
                               transforms=sas_transform,
                               train_bs=batch_size, val_bs=batch_size, test_bs=batch_size)
    datamodule.setup('fit')

    if model_type == "moco":
        model = MoCo(lr=lr, num_channels=num_channels, batch_size=batch_size, arch=backbone_type,
                     weight_decay=weight_decay, momentum=momentum, warmup_epochs=warmup_epochs,
                     temperature=temperature, memory_bank=memory_bank, max_epochs=max_epochs, num_train_samples=len(datamodule.train_dataset))
    else:
        print("Model not implemented")
        return

    trainer = pl.Trainer(
        accelerator="auto",
        devices=torch.cuda.device_count(),
        strategy=DDPStrategy(find_unused_parameters=False),
        sync_batchnorm=True,
        use_distributed_sampler=True,
        logger=logger,
        max_epochs=max_epochs,
        callbacks=[
            TQDMProgressBar(refresh_rate=20),
            ModelCheckpoint(dirpath="checkpoints", save_weights_only=False, save_top_k=1, mode="min",
                            monitor="train_loss", filename=model_type + "-{epoch:02d}-{train_loss:.2f}"),
            EarlyStopping(monitor="train_loss", min_delta=0.00, patience=10),
            LearningRateMonitor("epoch"),
        ])

    try:
        trainer.fit(model, datamodule=datamodule)
    except Exception as e:
        print(str(e))
    finally:
        logger.experiment.finish()



if __name__ == '__main__':
    args = parser.parse_args()
    torch.set_float32_matmul_precision('high')
    warnings.filterwarnings("ignore")
    pl.seed_everything(args.seed)

    sampler = GridSampler(grid)
    study = optuna.create_study(sampler=sampler)
    study.optimize(pretrain, n_trials=2)


