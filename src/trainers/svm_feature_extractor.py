import numpy as np
import optuna
import plotly.graph_objects as go
import pytorch_lightning as pl
import torch
import wandb
import warnings
from itertools import product
from matplotlib import pyplot as plt
from sklearn import svm
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import pairwise_distances
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from torch.utils.data import Subset
from torchmetrics import Recall, F1Score, Accuracy, Precision
from typing import Literal

from src.datasets.SASAugmentations import SASDataAugmentation
from src.datasets.SASDatamodule import SASDataModule
from src.models.moco.mocov2 import MoCo
from src.utils.plotting import plot_curves
from src.utils.trainer_argparser import parser


class FeatureExtractor(pl.LightningModule):
    """
    A PyTorch Lightning module designed for feature extraction using a given backbone model.

    Attributes:
    - backbone (torch.nn.Module): The pretrained model used for extracting features.

    Methods:
    - forward(x: torch.Tensor) -> torch.Tensor: Passes the input tensor through the backbone to obtain the feature embeddings.

    Notes:
    - The parameters of the backbone are frozen during initialization to ensure that they are not updated during any subsequent training.
    """

    def __init__(self, backbone):
        super().__init__()
        self.backbone = backbone

        for param in backbone.parameters():
            param.requires_grad = False

    def forward(self, x):
        return self.backbone(x).flatten(start_dim=1)

def visualize_nearest_neighbors(embeddings, dataset, query_idx=None, num_neighbors=5):
    """
    Visualize the nearest neighbors of a query image based on embeddings.

    Args:
    - embeddings (np.array): The feature embeddings of the images.
    - dataset (Dataset): The dataset containing the images.
    - query_idx (int, optional): The index of the query image. If None, a random image is selected.
    - num_neighbors (int, optional): Number of nearest neighbors to display.

    Returns:
    - None: Displays the plot, showcasing the query image and its nearest neighbors.

    Notes:
    - The function computes the pairwise distances between the query image and all other images based on their embeddings.
    - The nearest images are then displayed alongside the query image.
    """
    # If no query index provided, select a random one
    if query_idx is None:
        query_idx = np.random.randint(len(dataset))

    # Compute pairwise distances
    distances = pairwise_distances(embeddings[query_idx].reshape(1, -1), embeddings)[0]

    # Get indices of the nearest neighbors
    sorted_indices = np.argsort(distances)
    nearest_indices = sorted_indices[:num_neighbors + 1]  # +1 to account for the query image itself

    # Plot
    fig, axs = plt.subplots(1, num_neighbors + 1, figsize=(15, 5))

    # Display the query image
    query_img, _ = dataset[query_idx]
    axs[0].imshow(query_img[0], cmap='pink')  # Assumes single channel images
    #axs[0].set_title(f"Query Image (Idx: {query_idx})")
    axs[0].set_title(f"Query Image")
    axs[0].axis('off')

    # Display the nearest neighbors
    for ax, idx in zip(axs[1:], nearest_indices[1:]):  # Skip the first index since it's the query image
        img, label = dataset[idx]
        ax.imshow(img[0], cmap='pink')  # Assumes single channel image
        ax.set_title(f"Distance: {distances[idx]:.2f}")
        #ax.set_title(f"Dist: {distances[idx]:.2f}\nLabel: {label}")
        ax.axis('off')

    plt.tight_layout()
    plt.show()


def plot_svm_decision_boundary(X, y, model=None, ax=None):
    """
    Plot the decision boundary of an SVM on a 2D dataset.

    Args:
    - X (np.array): 2D data.
    - y (np.array): Labels.
    - model (SVM model, optional): Pretrained SVM. If None, a new SVM is trained.
    - ax (matplotlib axis, optional): Axis to plot on.

    Returns:
    - None: Displays the plot.

    Notes:
    - If an SVM model is not provided, a new SVM with a linear kernel is trained on the provided data.
    - The function creates a mesh grid and predicts on it to visualize the decision boundary.
    - Data points are displayed as scatter plots on top of the decision boundary.
    """
    if ax is None:
        fig, ax = plt.subplots()

    # Train SVM if not provided
    if model is None:
        model = SVC(kernel='linear')
        model.fit(X, y)

    # Create a mesh grid
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.02),
                         np.arange(y_min, y_max, 0.02))

    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    ax.contourf(xx, yy, Z, alpha=0.8)
    ax.scatter(X[:, 0], X[:, 1], c=y, edgecolors='k')
    ax.set_xlim(X[:, 0].min() - 0.5, X[:, 0].max() + 0.5)
    ax.set_ylim(X[:, 1].min() - 0.5, X[:, 1].max() + 0.5)
    ax.set_xlabel('PCA1')
    ax.set_ylabel('PCA2')
    ax.set_title('SVM Decision Boundary on 2D PCA Space')

class GridSampler(optuna.samplers.BaseSampler):
    """
    Custom sampler for hyperparameter optimization using Optuna.
    This sampler iterates over a predefined grid of hyperparameters.

    Attributes:
    - grid (dict): Dictionary containing the grid of hyperparameters.
    - current_trial (int): Index of the current trial in the grid.

    Methods:
    - infer_relative_search_space(study, trial) -> dict: Returns the predefined grid.
    - sample_relative(study, trial, search_space) -> dict: Samples the next set of hyperparameters from the grid.
    - sample_independent(study, trial, param_name, param_distribution): Raises an error as independent sampling is not implemented.
    """

    def __init__(self, grid):
        self.grid = grid
        # Convert the grid dictionary to a list of (name, values) pairs
        grid_items = list(grid.items())
        # Use itertools.product to generate all combinations
        self.grid_combinations = list(product(*[values for name, values in grid_items]))
        self.param_names = [name for name, values in grid_items]
        self.current_trial = 0

    def infer_relative_search_space(self, study, trial):
        return {name: optuna.distributions.CategoricalDistribution(values) for name, values in self.grid.items()}

    def sample_relative(self, study, trial, search_space):
        # Use the current combination of hyperparameters for this trial
        combination = self.grid_combinations[self.current_trial]
        self.current_trial += 1
        return {name: value for name, value in zip(self.param_names, combination)}

    def sample_independent(self, study, trial, param_name, param_distribution):
        # This method is used for parameters not contained in the search space.
        raise NotImplementedError

def test_visualization():
    """
    Utility function to test visualization using the SAS dataset and MoCo embeddings.

    Notes:
    - Sets up the SASDataModule.
    - Loads a pretrained MoCo model.
    - Computes embeddings and visualizations.
    - Uses PCA and t-SNE for dimensionality reduction.
    - Visualizes the SVM decision boundary in 2D and 3D spaces.
    """

    datamodule = SASDataModule(train_file=args.train_data, val_file=args.val_data,
                               test_file=args.test_data,
                               num_channels=1,
                               shuffle=False,
                               binary=True,
                               combine_test=True,
                               label_percentage=1.0,
                               transforms=SASDataAugmentation(apply_strong_augmentations=False),
                               train_bs=64, val_bs=64, test_bs=64)
    datamodule.setup()
    artifact_dir = '/home/bsheffield/git_repos/work_repos/ssl-vit-sas/checkpoints/moco-r18-1chan.ckpt'
    model = MoCo.load_from_checkpoint(artifact_dir, strict=False)
    backbone = model.backbone
    feature_extractor = FeatureExtractor(backbone)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    feature_extractor = feature_extractor.to(device)
    feature_extractor = feature_extractor.eval()
    sas_dataset = datamodule.test_dataset

    # filter out labels that are targets
    valid_indices = [i for i in range(len(sas_dataset)) if sas_dataset[i][1] != 1]
    sas_dataset = Subset(sas_dataset, valid_indices)

    datamodule.test_dataset = sas_dataset

    X_train = []
    y_train = []

    for batch in datamodule.test_dataloader():
        images, labels = batch
        images = images.to(device)
        features = feature_extractor(images)

        X_train.append(
            features.detach().cpu().numpy())  # Detach the features from the computation graph and move to CPU
        y_train.append(labels.cpu().numpy())

    # Convert list of arrays to a single numpy array
    X_train = np.concatenate(X_train, axis=0)
    y_train = np.concatenate(y_train, axis=0)

    visualize_nearest_neighbors(X_train, sas_dataset, query_idx=404, num_neighbors=5)

def plot_svm_decision_boundary_2D(X, y, model=None):
    """
    Plot the decision boundary of an SVM on a 2D dataset.

    Args:
    - X (np.array): 2D data.
    - y (np.array): Labels.
    - model (SVM model, optional): Pretrained SVM. If None, a new SVM is trained.

    Returns:
    - None: Displays the plot.
    """

    # Train SVM if not provided
    if model is None:
        model = SVC(kernel='linear', probability=True)
        model.fit(X, y)

    # Create a mesh grid
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.02),
                         np.arange(y_min, y_max, 0.02))

    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    # Create plotly 2D scatter and contour plot
    fig = go.Figure()

    # Scatter plot of data
    fig.add_trace(go.Scatter(x=X[:, 0], y=X[:, 1],
                             mode='markers',
                             marker=dict(color=y, size=3, opacity=0.6),
                             name='Data'))

    # Contour plot for decision boundary
    fig.add_trace(go.Contour(x=xx[0], y=yy[:, 0], z=Z, colorscale='Blues'))

    fig.update_layout(xaxis_title='t-SNE1', yaxis_title='t-SNE2', title='SVM Decision Boundary in 2D t-SNE Space')
    fig.show()


def plot_svm_decision_boundary_3D(X, y, model=None):
    """
    Plot the decision boundary of an SVM on a 3D dataset.

    Args:
    - X (np.array): 3D data.
    - y (np.array): Labels.
    - model (SVM model, optional): Pretrained SVM. If None, a new SVM is trained.

    Returns:
    - None: Displays the plot.

    Notes:
    - If an SVM model is not provided, a new SVM with a linear kernel is trained on the provided data.
    - The function creates a mesh grid in 3D space and predicts on it to visualize the decision boundary.
    - Data points are displayed as 3D scatter plots on top of the decision boundary.
    - Uses Plotly for 3D visualization.
    """

    # Train SVM if not provided
    if model is None:
        model = SVC(kernel='linear')
        model.fit(X, y)

    # Create a mesh grid
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    z_min, z_max = X[:, 2].min() - 1, X[:, 2].max() + 1
    xx, yy, zz = np.meshgrid(np.arange(x_min, x_max, 0.02),
                             np.arange(y_min, y_max, 0.02),
                             np.arange(z_min, z_max, 0.02))

    Z = model.predict(np.c_[xx.ravel(), yy.ravel(), zz.ravel()])
    Z = Z.reshape(xx.shape)

    # Create plotly 3D scatter plot
    fig = go.Figure()

    # Scatter plot of data
    fig.add_trace(go.Scatter3d(x=X[:, 0], y=X[:, 1], z=X[:, 2],
                               mode='markers',
                               marker=dict(color=y, size=3, opacity=0.6),
                               name='Data'))

    # Contour plot for decision boundary
    fig.add_trace(go.Isosurface(
        x=xx.flatten(),
        y=yy.flatten(),
        z=zz.flatten(),
        value=Z.flatten(),
        isomin=0.5,
        isomax=0.5,
        opacity=0.6,
        surface_count=1,
        colorscale='Blues',
        caps=dict(x_show=False, y_show=False, z_show=False)
    ))

    fig.update_layout(scene=dict(xaxis_title='t-SNE1', yaxis_title='t-SNE2', zaxis_title='t-SNE3'),
                      title='SVM Decision Boundary in 3D t-SNE Space')
    fig.show()


def finetune(trial):

    label_percentage = trial.relative_params['label_percentage']
    num_classes = trial.relative_params['num_classes']
    batch_size = trial.relative_params['batch_size']
    artifact_dir = trial.relative_params['ckpt']
    classifier_name = trial.relative_params['classifier_name']
    seed = trial.relative_params['seed']

    pl.seed_everything(seed)

    is_binary = True
    only_gt = False
    if num_classes > 1:
        is_binary = False
        only_gt = True

    model = MoCo.load_from_checkpoint(artifact_dir, strict=False)
    num_channels = model.hparams["num_channels"]
    backbone = model.backbone
    feature_extractor = FeatureExtractor(backbone)
    backbone_name = artifact_dir.split("/")[-1].split(".ckpt")[0]

    wandb.init(project="ssl-sas", name=f"trial_{trial.number}_" + f"{classifier_name}_" + str(int(label_percentage * 100)) + "%", tags=[backbone_name, classifier_name, str(int(label_percentage * 100)) + "%"], save_code=True)
    wandb.config["classifier_name"] = classifier_name
    wandb.config["backbone_name"] = backbone_name
    wandb.config["num_channels"] = num_channels
    wandb.config["num_classes"] = num_classes
    wandb.config["label_percentage"] = int(label_percentage * 100)
    datamodule = SASDataModule(train_file=args.train_data, val_file=args.val_data,
                               test_file=args.test_data,
                               num_channels=num_channels,
                               shuffle=True,
                               binary=is_binary,
                               only_gt=only_gt,
                               combine_test=True,
                               label_percentage=label_percentage,
                               transforms=SASDataAugmentation(apply_strong_augmentations=False),
                               train_bs=batch_size, val_bs=batch_size, test_bs=batch_size)
    datamodule.setup()

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    feature_extractor = feature_extractor.to(device)
    feature_extractor = feature_extractor.eval()

    X_train = []
    y_train = []

    for batch in datamodule.train_dataloader():
        images, labels = batch
        images = images.to(device)
        features = feature_extractor(images)

        X_train.append(
            features.detach().cpu().numpy())  # Detach the features from the computation graph and move to CPU
        y_train.append(labels.cpu().numpy())

    # Convert list of arrays to a single numpy array
    X_train = np.concatenate(X_train, axis=0)
    y_train = np.concatenate(y_train, axis=0)

    # Train a classifier on these features
    if classifier_name == 'LogisticRegression':
        clf = LogisticRegression()
    elif classifier_name == 'KNN':
        clf = KNeighborsClassifier()
    elif classifier_name == 'RandomForest':
        clf = RandomForestClassifier()
    elif classifier_name == 'DecisionTree':
        clf = DecisionTreeClassifier()
    elif classifier_name == 'GradientBoosting':
        clf = GradientBoostingClassifier()
    elif classifier_name == 'SVM':
        clf = svm.SVC(probability=True)
    elif classifier_name == 'AdaBoost':
        clf = AdaBoostClassifier()
    elif classifier_name == 'NaiveBayes':
        clf = GaussianNB()
    elif classifier_name == 'NeuralNetwork':
        clf = MLPClassifier()
    elif classifier_name == 'QDA':
        clf = QuadraticDiscriminantAnalysis()
    elif classifier_name == 'LDA':
        clf = LinearDiscriminantAnalysis()
    else:
        print("Unrecognized classifier!")
        return

    clf.fit(X_train, y_train)

    X_test = []
    y_test = []

    for batch in datamodule.test_dataloader():
        images, labels = batch
        images = images.to(device)
        features = feature_extractor(images)

        X_test.append(features.detach().cpu().numpy())
        y_test.append(labels.cpu().numpy())

    X_test = np.concatenate(X_test, axis=0)
    y_test = np.concatenate(y_test, axis=0)
    y_pred = clf.predict(X_test)

    # Incorrectly predicted indices
    incorrect_indices = np.where(y_pred != y_test)[0]

    # Pick 4 random indices from incorrect predictions
    rand_indices = np.random.choice(incorrect_indices, 4, replace=False)

    for i, idx in enumerate(rand_indices):
        image, true_label = datamodule.test_dataset[idx]
        pred_label = y_pred[idx]
        sas_img = image.numpy()

        fig, axs = plt.subplots(1, num_channels, figsize=(10, 5))

        # In case of single channel, axs will not be a list. So we convert it into a list.
        if num_channels == 1:
            axs = [axs]

        # use a for loop to iterate over each channel
        for channel in range(-num_channels, 0, 1):
            axs[channel].imshow(sas_img[channel], cmap='pink')
            axs[channel].set_title(f'Channel {abs(channel)} - Ground Truth: {true_label}, Pred: {pred_label}')
            axs[channel].axis('off')  # Hide axis values

        plt.tight_layout()
        plt.savefig(f"incorrect_pred_{i}.png")
        wandb.log({f"incorrect_predictions_{i}": wandb.Image(f"incorrect_pred_{i}.png")})

    plt.close('all')  # close all figures to free up memory

    task: Literal["binary", "multiclass"]

    if num_classes == 1:
        task = "binary"
    else:
        task = "multiclass"

    # Compute metrics
    if num_classes == 1:
        test_acc = Accuracy(num_classes=num_classes, task=task, pos_label=1)(torch.tensor(y_pred), torch.tensor(y_test))
        test_prec = Precision(num_classes=num_classes, task=task, pos_label=1)(torch.tensor(y_pred),
                                                                               torch.tensor(y_test))
        test_recall = Recall(num_classes=num_classes, task=task, pos_label=1)(torch.tensor(y_pred),
                                                                              torch.tensor(y_test))
        test_f1 = F1Score(num_classes=num_classes, task=task, pos_label=1)(torch.tensor(y_pred), torch.tensor(y_test))
    else:
        test_acc = Accuracy(num_classes=num_classes, task=task)(torch.tensor(y_pred), torch.tensor(y_test))
        test_prec = Precision(num_classes=num_classes, task=task)(torch.tensor(y_pred),
                                                                               torch.tensor(y_test))
        test_recall = Recall(num_classes=num_classes, task=task)(torch.tensor(y_pred),
                                                                              torch.tensor(y_test))
        test_f1 = F1Score(num_classes=num_classes, task=task)(torch.tensor(y_pred), torch.tensor(y_test))

    wandb.log({'test_acc': test_acc, 'test_precision': test_prec, 'test_recall': test_recall, 'test_f1': test_f1})

    y_probas = clf.predict_proba(X_test)  # Get the probabilities of the positive class

    wandb.sklearn.plot_learning_curve(clf, X_train, y_train)

    # This will give you the probabilities for the positive class, which can be used to plot the ROC and precision-recall curves.
    plot_curves(y_test, y_probas[:, 1])

    labels = np.unique(np.concatenate([y_train, y_test]))

    try:
        wandb.sklearn.plot_classifier(clf, X_train, X_test, y_train, y_test, y_pred, y_probas, labels, model_name=classifier_name)
    except ValueError as e:
        print(e)

    wandb.log({"total_train_samples": len(datamodule.train_dataset)})
    wandb.log({"total_test_samples": len(datamodule.test_dataset)})

    wandb.finish()

    return test_f1


if __name__ == '__main__':

    args = parser.parse_args()
    torch.set_float32_matmul_precision('high')
    warnings.filterwarnings("ignore")
    '''
    grid = {
        'seed': [args.seed],
        'num_classes': [args.num_classes],
        'batch_size': [args.batch_size],
        'classifier_name': ['SVM',],# 'LogisticRegression', 'KNN', 'RandomForest', 'DecisionTree', 'GradientBoosting', 'AdaBoost', 'NaiveBayes', 'NeuralNetwork'],
        'label_percentage':  [0.01, 0.05, 0.1, 0.5, 1.0],
        'ckpt': [
                "/home/sheffield/checkpoints/moco-r18-1chan.ckpt",
                #"/home/sheffield/checkpoints/moco-r34-1chan.ckpt",
                #"/home/sheffield/checkpoints/moco-r50-1chan.ckpt",
                 #"/home/sheffield/checkpoints/moco-r18-2chan.ckpt",
                 #"/home/sheffield/checkpoints/moco-r34-2chan.ckpt",
                #"/home/sheffield/checkpoints/moco-r50-2chan.ckpt"
                 ]
    }

    # Count the number of trials to perform for Grid Search
    sampler = GridSampler(grid)
    study = optuna.create_study(sampler=sampler, direction='maximize')
    n_trials = 1
    for param_values in grid.values():
        n_trials *= len(param_values)

    print(str(n_trials) + " grid search combinations to search")
    study.optimize(finetune, n_trials=n_trials, n_jobs=1)
    '''
    test_visualization()

