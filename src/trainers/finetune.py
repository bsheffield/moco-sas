import optuna
import pytorch_lightning as pl
import torch
import warnings
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
from pytorch_lightning.callbacks.progress import TQDMProgressBar
from pytorch_lightning.loggers.wandb import WandbLogger

from src.datasets.SASAugmentations import SASDataAugmentation
from src.datasets.SASDatamodule import SASDataModule
from src.models.fcn import ClassificationHead
from src.models.moco.mocov2 import MoCo
from src.utils.trainer_argparser import parser


class GridSampler(optuna.samplers.BaseSampler):
    """
    Custom sampler for hyperparameter optimization using Optuna.
    This sampler iterates over a predefined grid of hyperparameters.

    Attributes:
    - grid (dict): Dictionary containing the grid of hyperparameters.
    - current_trial (int): Index of the current trial in the grid.

    Methods:
    - infer_relative_search_space(study, trial) -> dict: Returns the predefined grid.
    - sample_relative(study, trial, search_space) -> dict: Samples the next set of hyperparameters from the grid.
    - sample_independent(study, trial, param_name, param_distribution): Raises an error as independent sampling is not implemented.
    """

    def __init__(self, grid):
        self.grid = grid
        self.current_trial = 0

    def infer_relative_search_space(self, study, trial):
        return self.grid

    def sample_relative(self, study, trial, search_space):
        values = {}
        for param_name, param_values in self.grid.items():
            values[param_name] = param_values[self.current_trial % len(param_values)]
        self.current_trial += 1
        return values

    def sample_independent(self, study, trial, param_name, param_distribution):
        # This method is used for parameters not contained in the search space.
        # You can return a default value or raise an error here.
        raise NotImplementedError

grid = {
    'lr': [0.0003],
    'batch_size': [64],
    'num_channels': [1],
    'label_percentage': [0.01, 0.05, 0.1, 1.0],
    'weight_decay': [0.0001],
    'max_epochs': [100],
    'model': ['moco'],
    'arch': ['resnet18']
}

def finetune(trial):
    """
    Fine-tuning function for the given model using the SAS dataset and PyTorch Lightning.

    Parameters:
    - trial (optuna.Trial): Optuna trial object containing hyperparameters.

    Notes:
    - Sets up the model, data module, and trainer.
    - Trains and tests the model.
    - Logs results and metrics using Wandb.
    """

    lr = trial.relative_params['lr']
    batch_size = trial.relative_params['batch_size']
    num_channels = trial.relative_params['num_channels']
    label_percentage = trial.relative_params['label_percentage']
    weight_decay = trial.relative_params['weight_decay']
    max_epochs = trial.relative_params['max_epochs']
    model_type = trial.relative_params['model']
    arch_type = trial.relative_params['arch']

    logger = WandbLogger(project="ssl-sas", log_model=True, name=str(model_type + ": " + str(label_percentage) + "%"), job_type="finetune")
    monitor_metric = "train_loss"
    objective_metric = "min"

    if args.local:
        artifact_dir = args.checkpoint
    else:
        artifact = logger.experiment.use_artifact(args.checkpoint, type='model')
        artifact_dir = artifact.download()
        artifact_dir += "/model.ckpt"

    if model_type == "moco":
        model = MoCo.load_from_checkpoint(artifact_dir, arch=arch_type, num_channels=num_channels, strict=False)
        model = ClassificationHead(model, lr=lr, arch=arch_type, batch_size=batch_size, max_epochs=max_epochs, weight_decay=weight_decay)

    trainer = pl.Trainer(
        accelerator="auto",
        devices=1,
        logger=logger,
        fast_dev_run=args.fast_dev_run,
        max_epochs=max_epochs,
        callbacks=[TQDMProgressBar(refresh_rate=20),
                   ModelCheckpoint(save_weights_only=False, save_top_k=1, mode=objective_metric, monitor=monitor_metric,
                                   filename=model_type + "-{epoch:02d}-" + "{" + monitor_metric + ":.2f}"),
                   LearningRateMonitor("epoch"),
                   ]
    )
    sas_transform = SASDataAugmentation(apply_strong_augmentations=False)
    datamodule = SASDataModule(train_file=args.train_data, val_file=args.val_data, test_file=args.test_data,
                               num_channels=num_channels, only_gt=False, binary=True, shuffle=True,
                               transforms=sas_transform,
                               label_percentage=label_percentage,
                               train_bs=batch_size, val_bs=batch_size, test_bs=batch_size)
    try:
        trainer.fit(model, datamodule=datamodule)
        trainer.test(model, datamodule=datamodule)
    except Exception as e:
        print(f"An error occurred: {str(e)}")
    finally:
        logger.experiment.finish()


if __name__ == '__main__':
    args = parser.parse_args()
    torch.set_float32_matmul_precision('high')
    warnings.filterwarnings("ignore")
    pl.seed_everything(args.seed)

    sampler = GridSampler(grid)
    study = optuna.create_study(sampler=sampler)
    study.optimize(finetune, n_trials=4)