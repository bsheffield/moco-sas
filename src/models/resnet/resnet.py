import numpy as np
import pytorch_lightning as pl
import time
import torch
import torch.nn as nn
import torchvision
import warnings
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
from pytorch_lightning.callbacks.progress import TQDMProgressBar
from pytorch_lightning.loggers.wandb import WandbLogger
from torchmetrics import ConfusionMatrix
from torchmetrics import Recall, F1Score, Accuracy, Precision
from typing import Literal

from src.datasets.SASDatamodule import SASDataModule
from src.models.moco.mocov2 import MoCo
from src.utils.plotting import plot_batch_images, plot_conf_matrix, plot_curves
from src.utils.trainer_argparser import parser


class ResNetClassifier(pl.LightningModule):
    """
    A PyTorch Lightning module for a ResNet-based classifier.

    Attributes:
    - num_channels (int): Number of input channels.
    - num_classes (int): Number of output classes.
    - lr (float): Learning rate for optimization.
    - weight_decay (float): Weight decay for optimization.
    - max_epochs (int): Maximum number of training epochs.
    - arch (str): Type of ResNet architecture (e.g., "resnet18", "resnet34").
    - pretrained_weights (str, optional): Path to pretrained model weights.

    Notes:
    - Initializes the ResNet architecture based on the provided type.
    - Loads pretrained weights if provided.
    - Sets up loss functions, optimizers, and other necessary components for training and inference.
    """

    def __init__(self, num_channels=2, num_classes=1, lr=3e-4, weight_decay=3e-4, max_epochs=100, arch="resnet18", pretrained_weights=None):
        super().__init__()
        self.save_hyperparameters()

        if arch == "resnet34":
            self.model = torchvision.models.resnet34(pretrained=False)
        elif arch == "resnet50":
            self.model = torchvision.models.resnet50(pretrained=False)
        else:
            self.model = torchvision.models.resnet18(pretrained=False)

        # ResNet v1.5: replace the first 7x7 convolution by a 3x3 convolution to make the model faster and run better on smaller input image resolutions.
        #self.model.conv1 = torch.nn.Conv2d(num_channels, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.model.conv1 = nn.Conv2d(num_channels, 64, kernel_size=7, stride=2, padding=3, bias=False)
        num_features = self.model.fc.in_features
        self.model.fc = torch.nn.Linear(num_features, num_classes)

        if pretrained_weights:
            pretrained_model = MoCo.load_from_checkpoint(pretrained_weights, strict=False).backbone.state_dict()
            model_dict = self.model.state_dict()  # Get the model's state dict
            # Filter out unnecessary keys from pretrained_dict
            pretrained_dict = {k: v for k, v in pretrained_model.items() if k in model_dict}
            # Overwrite entries in the existing state dict
            model_dict.update(pretrained_dict)
            self.model.load_state_dict(model_dict)

        task: Literal["binary", "multiclass"]

        if num_classes == 1:
            self.criterion = nn.BCEWithLogitsLoss()
            task = "binary"
        else:
            self.criterion = nn.CrossEntropyLoss()
            task = "multiclass"

        self.train_recall, self.val_recall = Recall(num_classes=num_classes, task=task), Recall(num_classes=num_classes, task=task)
        self.train_f1, self.val_f1 = F1Score(num_classes=num_classes, task=task), F1Score(num_classes=num_classes, task=task)
        self.train_acc, self.val_acc = Accuracy(num_classes=num_classes, task=task), Accuracy(num_classes=num_classes, task=task)
        self.train_precision, self.val_precision = Precision(num_classes=num_classes, task=task), Precision(num_classes=num_classes, task=task)

        self.test_acc = Accuracy(num_classes=num_classes, task=task)
        self.test_precision = Precision(num_classes=num_classes, task=task)
        self.test_recall = Recall(num_classes=num_classes, task=task)
        self.test_f1 = F1Score(num_classes=num_classes, task=task)
        self.test_confmat = ConfusionMatrix(num_classes=num_classes, task=task)

        self.y_true_all = []
        self.y_probs_all = []
        self.features_all = []

    def forward(self, x):

        output = self.model(x).flatten(start_dim=1)
        output = output.squeeze()
        return output

    def process_batch(self, batch, mode):
        x, y_true = batch
        start_time = time.time()
        y_preds = self(x)
        end_time = time.time()
        inference_time = end_time - start_time
        self.log(f"Inference Time", inference_time, logger=True)

        if isinstance(self.criterion, nn.CrossEntropyLoss):
            loss = self.criterion(y_preds, y_true.long())  # use long tensor for CrossEntropyLoss
        else:
            loss = self.criterion(y_preds, y_true.float())  # use float tensor for other losses
        y_probs = torch.sigmoid(y_preds)

        metrics = {
            "recall": getattr(self, f"{mode}_recall"),
            "f1": getattr(self, f"{mode}_f1"),
            "acc": getattr(self, f"{mode}_acc"),
            "precision": getattr(self, f"{mode}_precision"),
        }

        for name, metric in metrics.items():
            self.log(f"{mode}_{name}", metric(y_probs, y_true), on_step=False, on_epoch=True, prog_bar=True,
                     logger=True)

        self.log(f"{mode}_loss", loss, logger=True, prog_bar=True)

        return loss, y_probs, y_true

    def training_step(self, batch, batch_idx):
        loss, y_probs, y_true = self.process_batch(batch, "train")
        return loss

    def validation_step(self, batch, batch_idx):
        loss, y_probs, y_true = self.process_batch(batch, "val")
        return loss

    def test_step(self, batch, batch_idx):
        loss, y_probs, y_true = self.process_batch(batch, "test")

        self.y_true_all.append(y_true.cpu().numpy())
        self.y_probs_all.append(y_probs.cpu().numpy())

        y_pred = torch.round(y_probs)
        self.test_confmat.update(y_pred, y_true)

        plot_batch_images(batch[0], y_true, y_probs, self.logger, num_images=4)

        return loss

    def on_test_epoch_end(self) -> None:

        y_true = np.concatenate(self.y_true_all, axis=0)
        y_probs = np.concatenate(self.y_probs_all, axis=0)

        plot_curves(y_true, y_probs)

        confmat = self.test_confmat.compute().cpu().numpy()
        plot_conf_matrix(confmat)

        # Reset the lists for the next epoch
        self.y_true_all = []
        self.y_probs_all = []
        self.features_all = []

    def predict_step(self, batch, batch_idx, dataloader_idx=None):
        x, y = batch
        y_pred = self(x)
        return y_pred

    def configure_optimizers(self):
        optim = torch.optim.AdamW(self.parameters(), lr=self.hparams.lr, weight_decay=self.hparams.weight_decay)
        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optim, T_max=self.hparams.max_epochs)
        return [optim], [lr_scheduler]

if __name__ == '__main__':
    args = parser.parse_args()
    torch.set_float32_matmul_precision('high')
    warnings.filterwarnings("ignore")
    pl.seed_everything(args.seed)
    logger = WandbLogger(project="ssl-sas", log_model=True, name="ResNet18", job_type="train")

    from pytorch_lightning.strategies import DDPStrategy
    from src.datasets.SASAugmentations import SASDataAugmentation

    artifact_dir = "/home/sheffield/git_repos/ssl-vit-sas/moco-r18.ckpt"

    trainer = pl.Trainer(
        accelerator="auto",
        devices=1,
        fast_dev_run=args.fast_dev_run,
        max_epochs=args.max_epochs,
        logger=logger,
        callbacks=[
            TQDMProgressBar(refresh_rate=20),
            ModelCheckpoint(dirpath="checkpoints", save_weights_only=False, save_top_k=1, mode="min",
                            monitor="train_loss", filename="ResNet" + "-{epoch:02d}-{train_loss:.2f}"),
            LearningRateMonitor("epoch"),
        ])
    datamodule = SASDataModule(train_file=args.train_data, val_file=args.val_data, test_file=args.test_data,
                               num_channels=args.num_channels,
                               shuffle=True,
                               binary=True,
                               transforms=SASDataAugmentation(apply_strong_augmentations=False),
                               train_bs=args.batch_size, val_bs=args.batch_size, test_bs=args.batch_size)
    model = ResNetClassifier(max_epochs=args.max_epochs, num_channels=args.num_channels, num_classes=1, pretrained_weights=artifact_dir)
    trainer.fit(model, datamodule=datamodule)
    trainer.test(model, datamodule=datamodule)
