"""
This module contains functionalities related to the MoCo v2 algorithm and associated utilities.
It provides structures and methods for training and evaluating models using the MoCo v2 approach.
"""

import copy
import pytorch_lightning as pl
import torch
import torch.nn as nn
import torchvision
from lightly.loss import NTXentLoss
from lightly.models.modules import MoCoProjectionHead
from lightly.models.utils import update_momentum
from lightly.utils.debug import std_of_l2_normalized
from lightly.utils.scheduler import cosine_schedule
from pytorch_lightning.loggers import WandbLogger
from typing import Any

from src.datasets.SASAugmentations import SASMultiViewTransform


class MoCo(pl.LightningModule):

    def __init__(self,
                 input_dim=512,
                 hidden_dim=512,
                 output_dim=128,
                 momentum=0.999,
                 lr=0.0003,
                 arch="resnet18",
                 batch_size=256,
                 num_train_samples=500000,
                 max_epochs=100,
                 temperature=0.07,
                 memory_bank=4096,
                 weight_decay=1e-4,
                 num_channels=2,
                 warmup_epochs=10):
        """
        Initialize the MoCo model with the specified parameters.

        Parameters:
        - input_dim (int): Dimension of the input features.
        - hidden_dim (int): Dimension of the hidden layer in the projection head.
        - output_dim (int): Dimension of the output embeddings from the projection head.
        - momentum (float): Momentum factor for the momentum encoder.
        - lr (float): Learning rate.
        - arch (str): Backbone architecture ("resnet18", "resnet34", or "resnet50").
        - batch_size (int): Training batch size.
        - num_train_samples (int): Number of training samples.
        - max_epochs (int): Maximum number of training epochs.
        - temperature (float): Temperature factor for the NT-Xent loss.
        - memory_bank (int): Size of the memory bank for negative samples.
        - weight_decay (float): Weight decay factor.
        - num_channels (int): Number of input channels.
        - warmup_epochs (int): Number of warm-up epochs.

        Notes:
        1. Based on the chosen architecture (arch), the appropriate ResNet variant is loaded.
        2. The projection head for MoCo is initialized.
        3. The momentum encoder (both backbone and projection head) is created as a deep copy of the main encoder and is set to not require gradients.
        4. The NT-Xent loss with the specified temperature and memory bank size is initialized.
        """

        super().__init__()
        self.save_hyperparameters()

        if arch == "resnet34":
            resnet = torchvision.models.resnet34(pretrained=False)
            self.projection_head = MoCoProjectionHead(input_dim, hidden_dim, output_dim)
        elif arch == "resnet50":
            resnet = torchvision.models.resnet50(pretrained=False)
            self.projection_head = MoCoProjectionHead(input_dim=2048, hidden_dim=hidden_dim, output_dim=output_dim)
        elif arch == "resnet18":
            resnet = torchvision.models.resnet18(pretrained=False)
            self.projection_head = MoCoProjectionHead(input_dim, hidden_dim, output_dim)
        else:
            print("Unsupported backbone")
            exit(1)

        resnet.conv1 = nn.Conv2d(num_channels, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.backbone = nn.Sequential(*list(resnet.children())[:-1])
        self.backbone_momentum = copy.deepcopy(self.backbone)
        self.projection_head_momentum = copy.deepcopy(self.projection_head)

        for p in [*self.backbone_momentum.parameters(), *self.projection_head_momentum.parameters()]:
            p.requires_grad = False

        self.criterion = NTXentLoss(temperature=temperature, memory_bank_size=memory_bank, gather_distributed=True)

    def forward(self, x):
        """
        Forward pass of the MoCo model to obtain query embeddings.

        Parameters:
        - x (torch.Tensor): Input tensor of shape (batch_size, num_channels, height, width).

        Returns:
        - torch.Tensor: Query embeddings of shape (batch_size, output_dim).

        Notes:
        1. The input tensor is first passed through the backbone to extract features.
        2. The features are then flattened and passed through the projection head to obtain the query embeddings.
        """
        query = self.projection_head(self.backbone(x).flatten(start_dim=1))
        return query

    def forward_momentum(self, x):
        """
        Forward pass of the momentum encoder in the MoCo model to obtain key embeddings.

        Parameters:
        - x (torch.Tensor): Input tensor of shape (batch_size, num_channels, height, width).

        Returns:
        - torch.Tensor: Key embeddings of shape (batch_size, output_dim).

        Notes:
        1. The input tensor is first passed through the momentum-encoded backbone to extract features.
        2. The features are then flattened and passed through the momentum-encoded projection head to obtain the key embeddings.
        3. The key embeddings are detached to prevent gradient computations.
        """
        key = self.projection_head_momentum(self.backbone_momentum(x).flatten(start_dim=1)).detach()
        return key

    def training_step(self, batch, batch_idx):
        """
        Training step for the MoCo model. Defines the operations performed during one training iteration.

        Parameters:
        - batch (tuple): Input batch containing the data. The batch should contain a tuple of query and key images.
        - batch_idx (int): Index of the current batch.

        Returns:
        - torch.Tensor: Loss value for the current training step.

        Notes:
        1. The momentum for the momentum encoder is updated using a cosine schedule based on the current epoch.
        2. The momentum encoder (both backbone and projection head) is updated with the current momentum.
        3. The query and key images are extracted from the batch and passed through their respective encoders.
        4. The NT-Xent loss is computed using the resulting query and key embeddings.
        5. The standard deviation of L2-normalized query embeddings is logged to monitor the quality of representations.
           Values close to zero may indicate representation collapse.
        6. The training loss is logged for monitoring.
        """
        momentum = cosine_schedule(self.current_epoch, self.hparams.max_epochs, self.hparams.momentum, 1)
        update_momentum(self.backbone, self.backbone_momentum, m=momentum)
        update_momentum(self.projection_head, self.projection_head_momentum, m=momentum)

        (x_query, x_key) = batch[0]

        query = self.forward(x_query)
        key = self.forward_momentum(x_key)
        loss = self.criterion(query, key)

        # we want representations to move away from 0; close to zero means collapse!
        self.log("train_std_dev", std_of_l2_normalized(query), logger=True, prog_bar=True)
        self.log("train_loss", loss, logger=True, prog_bar=True)

        return loss

    def configure_optimizers(self):
        """
        Configure the optimizer and learning rate scheduler for training the MoCo model.

        Notes:
        - The method uses the AdamW optimizer with weight decay.
        - The learning rate scheduler is based on the OneCycleLR policy, which combines a linear warm-up phase followed by a cosine annealing phase.
        - The warm-up period helps to avoid overly large updates at the start of training by gradually increasing the learning rate.

        Returns:
        - list: A list containing the optimizer.
        - list: A list containing the learning rate scheduler.
        """

        optim = torch.optim.AdamW(self.parameters(), lr=0, weight_decay=self.hparams.weight_decay)
        steps_per_epoch = self.hparams.num_train_samples // self.hparams.batch_size
        lr_scheduler = torch.optim.lr_scheduler.OneCycleLR(
            optim,
            max_lr=self.hparams.lr,
            epochs=self.hparams.max_epochs,
            steps_per_epoch=steps_per_epoch,
            pct_start=self.hparams.warmup_epochs / self.hparams.max_epochs,
            anneal_strategy='cos',
            div_factor=25  # Starting learning rate will be max_lr / div_factor
        )
        return [optim], [lr_scheduler]

    def predict(self, x):
        """
        Predict the output for a given input tensor using the trained MoCo model.

        Parameters:
        - x (numpy.ndarray): Input data array of shape (num_channels, height, width).

        Returns:
        - float: Predicted output value after applying the sigmoid function.

        Notes:
        - The method first converts the numpy input array to a PyTorch tensor.
        - If the model is on GPU, the input tensor is moved to GPU.
        - The forward pass is done in evaluation mode and without computing gradients.
        - The predicted value is passed through the sigmoid function to obtain a value between 0 and 1.
        """

        with torch.no_grad():
            self.eval()  # Set the model to evaluation mode
            x = torch.from_numpy(x).float().unsqueeze(0)
            if next(self.parameters()).is_cuda:
                x = x.cuda()
            y_pred = self(x).squeeze()
            y_pred = torch.sigmoid(y_pred)
            return y_pred.item()

    def predict_step(self, batch: Any, batch_idx: int, dataloader_idx: int = 0) -> Any:
        """
        Predict step for the MoCo model. Defines the operations performed during one prediction iteration.

        Parameters:
        - batch (Any): Input batch containing the data.
        - batch_idx (int): Index of the current batch.
        - dataloader_idx (int, optional): Index of the dataloader. Defaults to 0.

        Returns:
        - Any: Predicted output for the given input batch.

        Notes:
        - The method simply forwards the input batch through the model to get the predictions.
        """

        return self(batch)


if __name__ == '__main__':

    import warnings
    from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor, EarlyStopping
    from pytorch_lightning.strategies.ddp import DDPStrategy
    from src.datasets.SASDatamodule import SASDataModule
    from pytorch_lightning.callbacks.progress import TQDMProgressBar
    from src.utils.trainer_argparser import parser

    # Parsing command-line arguments and setting configurations
    args = parser.parse_args()
    torch.set_float32_matmul_precision('high')
    warnings.filterwarnings("ignore")
    pl.seed_everything(args.seed)
    logger = WandbLogger(project="ssl-sas", log_model=True, name="MoCov2", job_type="train")

    # Initializing the PyTorch Lightning Trainer with specified parameters and callbacks
    trainer = pl.Trainer(
        accelerator="auto",
        devices=torch.cuda.device_count(),
        strategy=DDPStrategy(find_unused_parameters=False),
        use_distributed_sampler=True,
        sync_batchnorm=True,
        num_nodes=args.num_nodes,
        fast_dev_run=args.fast_dev_run,
        max_epochs=args.max_epochs,
        logger=logger,
        callbacks=[
            TQDMProgressBar(refresh_rate=20),
            ModelCheckpoint(dirpath="checkpoints", save_weights_only=False, save_top_k=1, mode="min",
                            monitor="train_loss", filename="MoCov2" + "-{epoch:02d}-{train_loss:.2f}"),
            EarlyStopping(monitor="train_loss", min_delta=0.00, patience=10),
            LearningRateMonitor("epoch"),
        ])

    # Initializing the data module for loading and preprocessing the SAS dataset
    datamodule = SASDataModule(train_file=args.train_data, val_file=args.val_data, test_file=args.test_data,
                               num_channels=args.num_channels,
                               shuffle=True,
                               binary=True,
                               only_gt=False,
                               transforms=SASMultiViewTransform(),
                               train_bs=args.batch_size, val_bs=args.batch_size, test_bs=args.batch_size)
    datamodule.setup()

    # Initializing the MoCo model with specified hyperparameters
    model = MoCo(max_epochs=args.max_epochs, batch_size=args.batch_size, lr=args.lr, weight_decay=args.weight_decay,
                 num_train_samples=len(datamodule.train_dataset), num_channels=args.num_channels)

    # Starting the training process
    trainer.fit(model, datamodule=datamodule)
