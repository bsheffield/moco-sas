import time
import numpy as np
import pytorch_lightning as pl
import torch.nn as nn
from pytorch_lightning.loggers import WandbLogger
from torchmetrics import Recall, F1Score, Accuracy, Precision
import torch
from torchmetrics import ConfusionMatrix
from src.datasets.SASAugmentations import SASDataAugmentation
from src.models.nnclr.nnclr import NNCLR
from src.utils.plotting import plot_batch_images, plot_curves, plot_ig
from captum.attr import IntegratedGradients
from typing import Literal

class ClassificationHead(pl.LightningModule):
    def __init__(self, backbone, num_classes=1, arch=None, output_dim=128, batch_size=256, lr=1e-4, weight_decay=1e-4,
                 max_epochs=100, label_percentage=1.0):
        """
         Initialize the ClassificationHead module with the given parameters.

         Parameters:
         - backbone (torch.nn.Module): The backbone network/model.
         - num_classes (int, optional): Number of output classes. Default is 1.
         ...

         Notes:
         - Initializes the classification head with a linear layer.
         - Sets the appropriate loss function based on the number of classes (binary or multiclass).
         - Initializes various metrics for training, validation, and testing.
         """

        super().__init__()
        self.save_hyperparameters()
        self.backbone = backbone
        self.fc = nn.Linear(output_dim, num_classes)

        for param in backbone.parameters():
            param.requires_grad = False

        task: Literal["binary", "multiclass"]

        if num_classes == 1:
            self.criterion = nn.BCEWithLogitsLoss()
            task = "binary"
        else:
            self.criterion = nn.CrossEntropyLoss()
            task = "multiclass"

        self.train_recall, self.val_recall = Recall(num_classes=num_classes, task=task), Recall(num_classes=num_classes, task=task)
        self.train_f1, self.val_f1 = F1Score(num_classes=num_classes, task=task), F1Score(num_classes=num_classes, task=task)
        self.train_acc, self.val_acc = Accuracy(num_classes=num_classes, task=task), Accuracy(num_classes=num_classes, task=task)
        self.train_precision, self.val_precision = Precision(num_classes=num_classes, task=task), Precision(num_classes=num_classes, task=task)

        self.test_acc = Accuracy(num_classes=num_classes, task=task)
        self.test_precision = Precision(num_classes=num_classes, task=task)
        self.test_recall = Recall(num_classes=num_classes, task=task)
        self.test_f1 = F1Score(num_classes=num_classes, task=task)

        self.test_confmat = ConfusionMatrix(num_classes=num_classes, task=task)

        self.y_true_all = []
        self.y_probs_all = []
        self.features_all = []

    def forward(self, x):
        """
        Forward pass of the ClassificationHead module.

        Parameters:
        - x (torch.Tensor): Input tensor of shape (batch_size, ...).

        Returns:
        - torch.Tensor: The model's output after the classification head.
        """

        y_hat = self.backbone(x).flatten(start_dim=1)
        return self.fc(y_hat)

    def process_batch(self, batch, mode):
        x, y_true = batch
        y_preds = self(x).squeeze()
        loss = self.criterion(y_preds, y_true.float())
        y_probs = torch.sigmoid(y_preds)

        metrics = {
            "recall": getattr(self, f"{mode}_recall"),
            "f1": getattr(self, f"{mode}_f1"),
            "acc": getattr(self, f"{mode}_acc"),
            "precision": getattr(self, f"{mode}_precision"),
        }

        for name, metric in metrics.items():
            self.log(f"{mode}_{name}", metric(y_probs, y_true), on_step=False, on_epoch=True, prog_bar=True,
                     logger=True)

        self.log(f"{mode}_loss", loss, logger=True, prog_bar=True)

        return loss, y_probs, y_true

    def training_step(self, batch, batch_idx):
        """
        Defines the training logic for a single batch.

        Parameters:
        - batch (Tuple[torch.Tensor, torch.Tensor]): Input batch of data and labels.
        - batch_idx (int): Index of the current batch.

        Returns:
        - torch.Tensor: Training loss for the current batch.
        """

        loss, y_probs, y_true = self.process_batch(batch, "train")
        return loss

    def validation_step(self, batch, batch_idx):
        """
        Defines the validation logic for a single batch.

        Parameters:
        - batch (Tuple[torch.Tensor, torch.Tensor]): Input batch of data and labels.
        - batch_idx (int): Index of the current batch.

        Returns:
        - torch.Tensor: Validation loss for the current batch.
        """

        loss, y_probs, y_true = self.process_batch(batch, "val")
        return loss

    def test_step(self, batch, batch_idx):
        """
        Defines the testing logic for a single batch.

        Parameters:
        - batch (Tuple[torch.Tensor, torch.Tensor]): Input batch of data and labels.
        - batch_idx (int): Index of the current batch.

        Returns:
        - torch.Tensor: Testing loss for the current batch.

        Notes:
        - Computes and logs inference time.
        - Updates various metrics and logs relevant information.
        """

        x, y_true = batch

        # ssl pretrain visualizations
        start_time = time.time()
        features = self.backbone(x).flatten(start_dim=1)
        end_time = time.time()
        inference_time = end_time - start_time
        self.log(f"Inference Time", inference_time, logger=True)
        self.features_all.append(features.cpu().numpy())

        loss, y_probs, y_true = self.process_batch(batch, "test")

        self.y_true_all.append(y_true.cpu().numpy())
        self.y_probs_all.append(y_probs.cpu().numpy())

        y_pred = torch.round(y_probs)
        self.test_confmat.update(y_pred, y_true)

        if batch_idx == 0:
            plot_batch_images(x, y_true, y_probs, self.logger, num_images=4)

        return loss

    def on_test_epoch_end(self) -> None:

        # visualize ssl pre-trained features
        features = np.concatenate(self.features_all, axis=0)
        y_true = np.concatenate(self.y_true_all, axis=0)
        y_probs = np.concatenate(self.y_probs_all, axis=0)

        #plot_features(features, y_true, self.logger, title="")
        plot_curves(y_true, y_probs)

        # Reset the lists for the next epoch
        self.y_true_all = []
        self.y_probs_all = []
        self.features_all = []

    def configure_optimizers(self):
        optim = torch.optim.AdamW(self.fc.parameters(), lr=self.hparams.lr, weight_decay=self.hparams.weight_decay)
        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optim, T_max=self.hparams.max_epochs)
        return [optim], [lr_scheduler]

    def predict_step(self, batch, batch_idx, dataloader_idx=None):

        sas_img, label = batch
        sas_img = sas_img.unsqueeze(0)

        y_preds = self(sas_img).squeeze()
        y_probs = torch.sigmoid(y_preds)

        integrated_gradients = IntegratedGradients(self)
        plot_ig(sas_img, y_probs, integrated_gradients)

        return y_probs



if __name__ == '__main__':
    artifact_dir = "/checkpoints/nnclr-1chan.ckpt"
    #artifact_dir = "/home/sheffield/git_repos/ssl-vit-sas/src/trainers/artifacts/model-h5cz8l5w:v0/model.ckpt"
    #model = MoCo.load_from_checkpoint(artifact_dir, strict=False)
    model = NNCLR.load_from_checkpoint(artifact_dir, strict=False)
    model = ClassificationHead(model.backbone, output_dim=512, num_classes=1)
    '''
    #model = model.to(device="cpu")

    sasdataset = SASDataset(hdf5_file_path="/mnt/data/hd2/sas/marc/test_balanced.h5")
    # Since predict_step expects a batch, we should add an extra dimension
    # Get a sample from your dataset
    sas_img, label = sasdataset[93]

    # Make a prediction and visualize integrated gradients
    model.eval()
    sas_img.requires_grad_(True)
    prediction = model.predict_step((sas_img, label), batch_idx=0)

    print(prediction)
    '''

    import warnings
    from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor
    from src.datasets.SASDatamodule import SASDataModule
    from pytorch_lightning.callbacks.progress import TQDMProgressBar
    from src.utils.trainer_argparser import parser

    args = parser.parse_args()
    torch.set_float32_matmul_precision('high')
    warnings.filterwarnings("ignore")
    pl.seed_everything(args.seed)
    logger = WandbLogger(project="ssl-sas", log_model=True, name="MoCov2FT", job_type="train")

    trainer = pl.Trainer(
        accelerator="gpu",
        devices=1,
        fast_dev_run=args.fast_dev_run,
        max_epochs=args.max_epochs,
        #precision="bf16",
        logger=logger,
        callbacks=[
            TQDMProgressBar(refresh_rate=20),
            ModelCheckpoint(dirpath="checkpoints", save_weights_only=False, save_top_k=1, mode="min",
                            monitor="train_loss", filename="MocoTest" + "-{epoch:02d}-{train_loss:.2f}"),
            # EarlyStopping(monitor="train_loss", min_delta=0.00, patience=10),
            LearningRateMonitor("epoch"),
        ])
    datamodule = SASDataModule(train_file=args.train_data, val_file=args.val_data, test_file=args.test_data,
                               num_channels=args.num_channels,
                               shuffle=True,
                               binary=True,
                               transforms=SASDataAugmentation(apply_strong_augmentations=False),
                               train_bs=args.batch_size, val_bs=args.batch_size, test_bs=args.batch_size)
    trainer.fit(model, datamodule=datamodule)
    trainer.test(model, datamodule=datamodule)


