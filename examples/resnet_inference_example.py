import os
import numpy as np
import torch
import matplotlib.pyplot as plt
import h5py
from src.models.resnet.resnet import ResNetClassifier
import wandb

def sliding_window(data, window_size=224):
    snippets = []
    # Get the shape of the data
    rows, cols = data.shape
    # Calculate the number of snippets in each dimension
    row_snippets = rows // window_size
    col_snippets = cols // window_size
    # Iterate over the data and extract the snippets
    for i in range(row_snippets):
        for j in range(col_snippets):
            snippet = data[i * window_size:(i + 1) * window_size, j * window_size:(j + 1) * window_size]
            snippets.append(snippet)
    return snippets

run = wandb.init()
artifact = run.use_artifact('bsheffield2008/ssl-sas/model-2rneh7wb:v0', type='model')
artifact_dir = artifact.download()
artifact_dir = artifact_dir + '/model.ckpt'
run.finish()

model = ResNetClassifier(num_channels=1).load_from_checkpoint(artifact_dir)

filename = "/mnt/data/hd2/unacorn/GT/extracted/SN06323-20211027-152249/ContactExport/H5Files/PHi00-21Oct27_1539-000001.h5"
confidence_score = 0.70
with h5py.File(filename, 'r') as f:
    ping_data = f['/Sonar/Sensor1/Data1/PingData'][:]
    ping_data_squeezed = np.squeeze(ping_data)
    snippets = sliding_window(ping_data_squeezed)
snippets_expanded = []
for snippet in snippets:
    snippet_expanded = np.expand_dims(snippet, axis=0)
    snippet_expanded = np.expand_dims(snippet_expanded, axis=0)
    snippets_expanded.append(snippet_expanded)

snippets_dir = "snippets"
os.makedirs(snippets_dir, exist_ok=True)

predictions_file = open(os.path.join(snippets_dir, 'predictions.txt'), 'w')

for i, snippet in enumerate(snippets_expanded):
    image_filename = os.path.join(snippets_dir, f'snippet_{i}.png')
    plt.imsave(image_filename, np.squeeze(snippet), cmap='pink')

    snippet = torch.from_numpy(snippet)
    snippet_tensor = snippet.float().to(model.device)
    result = model(snippet_tensor)
    result = torch.sigmoid(result)
    if result > confidence_score:
        print("Found a target at " + str(image_filename) + " with confidence: " + str(result))

    # Save prediction to file
    predictions_file.write(f'snippet_{i}.png: {result.item()}\n')

predictions_file.close()