import torch
import numpy as np
from src.datasets.SASAugmentations import SASDataAugmentation, SASMultiViewTransform
import torch.nn as nn
import pytorch_lightning as pl
import torchvision
import faiss
from src.models.moco.mocov2 import MoCo

model = MoCo()

# Assume that dataloader is a DataLoader that loads all your database images
embeddings = []
image_ids = []
model.eval()  # Set the model to evaluation mode
with torch.no_grad():
    for img, img_id in dataloader:
        embedding = model(img).numpy()
        embeddings.append(embedding)
        image_ids.append(img_id)

# Convert lists to arrays
embeddings = np.vstack(embeddings)
image_ids = np.hstack(image_ids)

# Normalize the embeddings
embeddings /= np.linalg.norm(embeddings, axis=1)[:, np.newaxis]


# Build the index
dimension = embeddings.shape[1]  # The dimension of the embeddings
index = faiss.IndexFlatL2(dimension)

# Adding vectors to the index
index.add(embeddings)


def retrieve(query_image, k=10):
    query_embedding = model(query_image).numpy()

    # Normalize the query embedding
    query_embedding /= np.linalg.norm(query_embedding)

    # Faiss search
    distances, indices = index.search(query_embedding[None, :], k)

    # Get the image ids of the most similar images
    similar_image_ids = image_ids[indices[0]]

    return similar_image_ids
