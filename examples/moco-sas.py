import h5py
import numpy as np
import pytorch_lightning as pl
import torch
from src.datasets.SASAugmentations import SASDataAugmentation
import warnings
from src.datasets.SASDatamodule import SASDataModule
from src.utils.trainer_argparser import parser
from src.models.moco.mocov2 import MoCo
from sklearn import svm

class FeatureExtractor(pl.LightningModule):
    def __init__(self, backbone):
        super().__init__()
        self.backbone = backbone

        for param in backbone.parameters():
            param.requires_grad = False

    def forward(self, x):
        return self.backbone(x).flatten(start_dim=1)

clf = svm.SVC(probability=True)
args = parser.parse_args()
parser.add_argument("hdf5_file")
artifact_dir = args.checkpoint
if args.checkpoint == None:
    print("Please provide a MoCo checkpoint file.")

hdf5_file_path = "/mnt/data/hd2/unacorn/GT/extracted/SN06222-20210728-091718 MISSION 1/H5Files/PHi00-21Jul28_1648-000004.h5"

model = MoCo.load_from_checkpoint(artifact_dir, strict=False)
num_channels = model.hparams["num_channels"]
backbone = model.backbone
feature_extractor = FeatureExtractor(backbone)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
feature_extractor = feature_extractor.to(device)
feature_extractor = feature_extractor.eval()

X_train = []
y_train = []
args = parser.parse_args()
torch.set_float32_matmul_precision('high')
warnings.filterwarnings("ignore")
datamodule = SASDataModule(train_file=args.train_data, val_file=args.val_data,
                           test_file=args.test_data,
                           num_channels=1,
                           shuffle=True,
                           binary=True,
                           label_percentage=0.01,
                           transforms=SASDataAugmentation(apply_strong_augmentations=False),
                           train_bs=64, val_bs=64, test_bs=64)
datamodule.setup()
for batch in datamodule.train_dataloader():
    images, labels = batch
    images = images.to(device)
    features = feature_extractor(images)

    X_train.append(
        features.detach().cpu().numpy())  # Detach the features from the computation graph and move to CPU
    y_train.append(labels.cpu().numpy())

# Convert list of arrays to a single numpy array
X_train = np.concatenate(X_train, axis=0)
y_train = np.concatenate(y_train, axis=0)

clf.fit(X_train, y_train)

X_test = []
y_test = []
num_positive_preds = 0
snippet_batch = 256
with h5py.File(hdf5_file_path, 'r') as hdf5_file:
    ping_data = hdf5_file['Sonar/Sensor1/Data1/PingData'][:]
    snippet_size = (224, 224)

    snippets = []
    snippet_count = 0

    for i in range(0, (ping_data.shape[0] - snippet_size[0] + 1)):
        for j in range(0, ping_data.shape[1] - snippet_size[1] + 1):
            snippet = ping_data[i:i + snippet_size[0], j:j + snippet_size[1]]
            snippets.append(snippet)
            snippet_count += 1

            # Perform inference every snippet_batch
            if snippet_count == snippet_batch:
                snippets = np.array(snippets)
                snippets = torch.from_numpy(snippets)
                snippets = snippets.to(device)  # Move the tensor to the same device as the model

                features = feature_extractor(snippets)
                X_test = features.detach().cpu().numpy()  # Get features for test
                y_probas = clf.predict_proba(X_test)  # Get the probabilities of the positive class
                y_preds = (y_probas[:, 1] > 0.5).astype(int)  # Convert probabilities to class predictions
                num_positive_preds += y_preds.sum()  # Count the number of positive predictions

                # Reset snippets and snippet_count for the next batch
                snippets = []
                snippet_count = 0

print(num_positive_preds)