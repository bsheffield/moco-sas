
# MoCo-SAS

## Overview

This repository contains code and utilities to employ self-supervised learning techniques for Synthetic Aperture Sonar (SAS) imagery. Specifically, it leverages MoCo v2 for contrastive learning and ResNet architectures for downstream classification tasks.

## Key Components

- **MoCo v2 Implementation**: A PyTorch implementation of the Momentum Contrast (MoCo) v2 algorithm for self-supervised learning.
- **ResNet Classifier**: A ResNet-based classifier designed for downstream tasks after pretraining with MoCo v2.
- **SAS Dataset and DataModule**: Utilities to load and preprocess the SAS dataset, including data augmentation techniques tailored for SAS imagery.
- **SVM Feature Extractor**: A utility to extract feature embeddings from a pretrained model and train SVM classifiers on the embeddings.

## Getting Started

### Setting Up the Anaconda Environment

This project is best run inside an Anaconda environment. If you have the environment set up and wish to reproduce it, follow the steps below:

1. **Install Anaconda or Miniconda**:
   - If you haven't already, download and install [Anaconda](https://www.anaconda.com/products/distribution) or [Miniconda](https://docs.conda.io/en/latest/miniconda.html).

2. **Recreate the moco-sas Environment**:
   To recreate the `moco-sas` environment from the provided `environment.yml` file, run:
   ```bash
   conda env create -f environment.yml
   ```

3. **Activate the moco-sas Environment**:
   ```bash
   conda activate moco-sas
   ```

### Training

1. **Pretraining with MoCo v2**:
   ```bash
   python pretrain.py
   ```

2. **Fine-tuning a Classifier**:
   ```bash
   python finetune.py
   ```

3. **Training ResNet Classifier**:
   ```bash
   python train_resnet.py
   ```

4. **SVM Classification**:
   ```bash
   python svm_feature_extractor.py
   ```

### Visualizations

The repository provides utilities to visualize nearest neighbors in the feature space and the decision boundaries of SVM classifiers. Refer to the `svm_feature_extractor.py` script for details.

## Contributing

If you're interested in contributing, please fork the repository and make changes as you'd like. Pull requests are warmly welcome.

## License

This project is licensed under the MIT License.

## Acknowledgments

- The MoCo v2 algorithm is based on the [official implementation](https://github.com/facebookresearch/moco).
- The SAS dataset preprocessing and augmentation techniques are tailored for sonar imagery and were developed in collaboration with domain experts.

